const urlRestApi = 'https://apipondasi.farmasys.com/';
var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = [
    '/',
    '/favicon.ico',
    '/src/manifest.json',
    '/src/app.js',

    '/src/views/pages/menu/marketing/farmasys/attendance/attendance.js',
    '/src/views/pages/menu/marketing/farmasys/attendance/attendance.html',
    '/src/views/pages/menu/marketing/farmasys/attendance/attendance-table.js',
    '/src/views/pages/menu/marketing/farmasys/attendance/attendance-table.html',
    '/src/views/pages/menu/marketing/farmasys/attendance/attendance-form.js',
    '/src/views/pages/menu/marketing/farmasys/attendance/attendance-form.html',
    '/src/views/pages/login/login.html',
    '/src/views/pages/login/login.js',
    '/src/views/pages/dashboard/dashboard.js',
    '/src/views/pages/dashboard/dashboard.html',

    '/src/views/components/topbar/topbar.js',
    '/src/views/components/topbar/topbar.html',
    '/src/views/components/sidebar/sidebar.js',
    '/src/views/components/sidebar/sidebar.html',

    '/src/models/models.js',

    '/src/assets/js/moment.min.js',
    '/src/assets/js/util.js',
    '/src/assets/js/sweetalert2.min.js',
    '/src/assets/js/select2.min.js',
    '/src/assets/js/jquery.validate.min.js',
    '/src/assets/js/jquery-3.4.1.min.js',
    '/src/assets/js/extension.js',
    '/src/assets/js/bootstrap.bundle.min.js',
    '/src/assets/js/bootstrap.bundle.min.js.map',
    '/src/assets/js/router/router.js',
    '/src/assets/js/router/route.js',
    '/src/assets/image/logo.png',
    '/src/assets/image/logo-square.png',
    '/src/assets/image/person-placeholder.jpg',
    '/src/assets/image/bg-login.webp',
    '/src/assets/image/icon/xxxxhdpi/ic_launcher.png',
    '/src/assets/image/icon/hdpi/ic_launcher.png',
    '/src/assets/image/icon/mdpi/ic_launcher.png',
    '/src/assets/image/icon/xhdpi/ic_launcher.png',
    '/src/assets/image/icon/xxhdpi/ic_launcher.png',
    '/src/assets/image/icon/xxxhdpi/ic_launcher.png',
    '/src/assets/css/select2.min.css',
    '/src/assets/css/select2-bootstrap4.min.css',
    '/src/assets/css/main.css',
    '/src/assets/css/hamburgers.min.css',
    '/src/assets/css/bootstrap.css.map',
    '/src/assets/css/bootstrap.css',
];

self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(CACHE_NAME).then(function (cache) {
            console.log('Opened cache');
            return cache.addAll(urlsToCache);
        })
    );
});

self.addEventListener('fetch', function (event) {
    var request = event.request;
    var url = new URL(request.url);
    if (url.origin == location.origin) {
        event.respondWith(
            caches.match(request).then(function (response) {
                return response || fetch(response)
            })
        )
    } else if (url.origin == urlRestApi) {
        event.respondWith(
            caches.open('dynamic').then(function (cache) {
                console.log(request);
                console.log(request.headers);
                console.log(request.headers.get('Authorization'));
                return fetch(request, { headers: request.headers }).then(function (response) {
                    cache.put(request, response.clone());
                    return response
                }).catch(function () {
                    return caches.match(request).then(function (response) {
                        if (response) return response;
                    })
                });
            })
        )
    }
});

self.addEventListener('activate', function (event) {
    event.waitUntil(
        caches.keys().then(function (cacheNames) {
            return Promise.all(
                cacheNames.filter(function (cacheName) {
                    return cacheName != CACHE_NAME;
                }).map(function (cacheName) {
                    return caches.delete(cacheName);
                })
            );
        })
    );
});