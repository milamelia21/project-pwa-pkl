if (!Auth.isLoggedIn()) {
    Auth.loggedOut()
} else {
    const user = Auth.userInfo();
    $('#topbar').topbar({
        user: {
            title: user.name,
            subtitle: user.role + ' - ' + user.nip,
            picture: 'https://demo.dashboardpack.com/architectui-angular-pro/assets/images/avatars/1.jpg',
        }
    });
    $('#sidebar').sidebar([{
            group: "General",
            menu: [{
                    name: 'City',
                    icon: 'location_city',
                    submenu: [{
                        name: "City",
                        route: 'administration/master/cities',
                    }, ]
                }
            ]
        }
    ]);
}