if (!Auth.isLoggedIn()) {
    Auth.loggedOut()
} else {
    const user = Auth.userInfo();
    $('#page-title').titlePage({
        icon: 'tag_faces',
        iconColor: ['#84fab0', '#8fd3f4'],
        title: 'Hello, ' + user.NAMA,
        subtitle: 'Welcome to Farmasys'
    });
}

$('#page-content').load('src/views/pages/menu/main-navigation/dashboard/analytics/analytics-charts.html')