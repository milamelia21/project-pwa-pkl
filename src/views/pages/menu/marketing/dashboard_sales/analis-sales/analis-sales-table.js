var periode = '';
var jabatan = '';
var kodeff = '';
var namacust = '';
$('#periode').val(periode);
$('#jabatan').val(jabatan);
$('#kodeff').val(kodeff);

var periodeinput = $("#periode")
var jabataninput = $("#jabatan")
var custinput = $("#kodeff")
$("#name").text("Nama GT")
var tabelAnalisa;

var mType = '';
var totalJan = 0;
var totalFeb = 0;
var totalMar = 0;
var totalApr = 0;
var totalMei = 0;
var totalJun = 0;
var totalJul = 0;
var totalAgs = 0;
var totalSep = 0;
var totalOkt = 0;
var totalNov = 0;
var totalDes = 0;

async function setTableCheckClaimDiscount() {
    $('#namaGT').hide()
    try {
        tabelAnalisa.destroy();
    } catch (error) {

    }
    $('#table-check-data tfoot').remove();
    await $('#table-check-data').tableSetDataFromAPI(async () => DashboardAnalisaSales.getAnalisaSalesAwal(periode, jabatan, kodeff), (item, i) => {
        totalJan += parseFloat(item.Jan);
        totalFeb += parseFloat(item.Feb);
        totalMar += parseFloat(item.Mar);
        totalApr += parseFloat(item.Apr);
        totalMei += parseFloat(item.Mei);
        totalJun += parseFloat(item.Jun);
        totalJul += parseFloat(item.Jul);
        totalAgs += parseFloat(item.Ags);
        totalSep += parseFloat(item.Sep);
        totalOkt += parseFloat(item.Okt);
        totalNov += parseFloat(item.Nov);
        totalDes += parseFloat(item.Des);
        $("#totalJan").html(toIDR(totalJan))
        $('#totalFeb').html(toIDR(totalFeb))
        $('#totalMar').html(toIDR(totalMar))
        $('#totalApr').html(toIDR(totalApr))
        $('#totalMei').html(toIDR(totalMei))
        $('#totalJun').html(toIDR(totalJun))
        $('#totalJul').html(toIDR(totalJul))
        $('#totalAgs').html(toIDR(totalAgs))
        $('#totalSep').html(toIDR(totalSep))
        $('#totalOkt').html(toIDR(totalOkt))
        $('#totalNov').html(toIDR(totalNov))
        $('#totalDes').html(toIDR(totalDes))
        if (i == 0) {
            $('#table-check-data').append(`
            <tfoot>
                <tr style="background-color:#F76BC0">
                    <th scope="row"></th>
                    <th scope="row">JUMLAH KESELURUHAN :</th>
                    <th scope="row"></th>
                    <th scope="row"></th>
                    <th scope="row" id="totalJan"></th>
                    <th scope="row" id="totalFeb"></th>
                    <th scope="row" id="totalMar"></th>
                    <th scope="row" id="totalApr"></th>
                    <th scope="row" id="totalMei"></th>
                    <th scope="row" id="totalJun"></th>
                    <th scope="row" id="totalJul"></th>
                    <th scope="row" id="totalAgs"></th>
                    <th scope="row" id="totalSep"></th>
                    <th scope="row" id="totalOkt"></th>
                    <th scope="row" id="totalNov"></th>
                    <th scope="row" id="totalDes"></th>
                    <th scope="row"></th>
                </tr>
            </tfoot>
        `)
        }
        return (
            `<tr style="background-color:#87CEFA">
            <td>${item.Periode}</td>
            <td>${item.NamaGT}</td>
            <td>${item.JabGT}</td>
            <td>${item.KodeArea}</td>
            <td>${item.Jan.toIDR()}</td>
            <td>${item.Feb.toIDR()}</td>
            <td>${item.Mar.toIDR()}</td>
            <td>${item.Apr.toIDR()}</td>
            <td>${item.Mei.toIDR()}</td>
            <td>${item.Jun.toIDR()}</td>
            <td>${item.Jul.toIDR()}</td>
            <td>${item.Ags.toIDR()}</td>
            <td>${item.Sep.toIDR()}</td>
            <td>${item.Okt.toIDR()}</td>
            <td>${item.Nov.toIDR()}</td>
            <td>${item.Des.toIDR()}</td>
            <td style="background-color:#48C9B0">${item.Total.toIDR()}</td>
            <td>
                <button class="btn btn-success btn-sm btn-icon-only" onclick="datadetail('${periodeinput.val()}','${jabataninput.val()}','${item.KodeArea}','','docter')" data-toggle="tooltip" title="Docter" type="button">
                        <i class="material-icons-outlined">face</i>
                </button>
                <button class="btn btn-primary btn-sm btn-icon-only" onclick="datadetail('${periodeinput.val()}','${jabataninput.val()}','${item.KodeArea}','product')" data-bs-toggle="tooltip" title="Product" type="button">
                    <i class="material-icons-outlined">layers</i>
                </button>
                <button class="btn btn-warning btn-sm btn-icon-only" onclick="datadetail('${periodeinput.val()}','${jabataninput.val()}','${item.KodeArea}','outlet')" data-bs-toggle="tooltip" title="Outlet" type="button">
                     <i class="material-icons-outlined">store</i>
                </button>
                <button class="btn btn-warning btn-sm btn-icon-only" onclick="datadetail('${periodeinput.val()}','${jabataninput.val()}','${item.KodeArea}','','call')" data-bs-toggle="tooltip" title="Call" type="button">
                     <i class="material-icons-outlined">settings_phone</i>
                </button>
                <button class="btn btn-danger btn-sm btn-icon-only" data-bs-toggle="tooltip" 
                onclick="datadocter('bawahan')" title="Bahawan" type="button">
                    <i class="material-icons-outlined">arrow_downward</i>
                </button>
            </td>
        </tr>
        `
        );
    });
    tabelAnalisa = $('#table-check-data').DataTable({
        "scrollX": true
    });
}
setTableCheckClaimDiscount();

$('select').comboboxSearch();
$('.date-picker').datepicker()

$('form[name="dashboard_sales"]').submit(function (e) {
    e.preventDefault();
    const form = e.currentTarget;
    periode = form.periode.value;
    jabatan = form.jabatan.value;
    kodeff = form.kodeff.value;
    if (mType == '')
        setTableCheckClaimDiscount();
    else
        datadetail(periode, jabatan, kodeff, namacust, mType);
});

var ComboboxJabatan = $('#jabatan')
var ComboboxFF = $('#kodeff')
var ComboboxPeriode = $('#periode')

ComboboxJabatan.setOptionsFromAPI({
    api: DashboardAnalisaSales.getmasterjabatab,
    text: 'MSM_KodeJab',
    value: 'MSM_Level'
});
ComboboxPeriode.setOptionsFromAPI({
    api: DashboardAnalisaSales.getmasterperiode,
    text: 'TSF_Periode',
    value: 'TSF_Periode'
});

ComboboxJabatan.change(function (e) {
    jabatan = e.currentTarget.value;
    currentjabatan = e.currentTarget.value;
    [ComboboxFF].forEach(e => e.empty())
    if (jabatan != undefined) {
        ComboboxFF.setOptionsFromAPI({
            api: async () => await DashboardAnalisaSales.getFF(jabatan, ComboboxPeriode.val()),
            text: 'MSM_KodeArea',
            value: 'MSM_KodeArea'
        });
    }
});

//Detail Product
let api;
async function datadetail(periode, jabatan, kodeff, namacust, type) {
    console.log('tes', type)
    try {
        mType = type;
        totalJan = 0;
        totalFeb = 0;
        totalMar = 0;
        totalApr = 0;
        totalMei = 0;
        totalJun = 0;
        totalJul = 0;
        totalAgs = 0;
        totalSep = 0;
        totalOkt = 0;
        totalNov = 0;
        totalDes = 0;
        $('[name=kodeff]').val(kodeff).trigger("change");
        tabelAnalisa.destroy();
        $('#table-check-data tfoot').remove();
        if (type === 'docter') {
            $("#name").text("Nama Cust")
            $('#namaGT').show()
            await $('#table-check-data').tableSetDataFromAPI(async () => DashboardAnalisaSales.getAnalisaSalesDocter(periode, jabatan, kodeff), (res, i) => handleResponse(res, i, type));
        } else if (type === 'product') {
            $("#name").text("Nama Product")
            console.log('type pp', type, { periode, jabatan, kodeff, namacust })
            $('#namaGT').show()
            await $('#table-check-data').tableSetDataFromAPI(async () => DashboardAnalisaSales.getAnalisaSalesProduct(periode, jabatan, kodeff, namacust), (res, i) => handleResponse(res, i, type));
        } else if (type === 'outlet') {
            $('#namaGT').show()
            $("#name").text("Nama Outlet")
            await $('#table-check-data').tableSetDataFromAPI(async () => DashboardAnalisaSales.getAnalisaSalesOutlet(periode, jabatan, kodeff, namacust), (res, i) => handleResponse(res, i, type));
        } else if (type === 'call') {
            $("#name").text("Nama Docter")
            await $('#table-check-data').tableSetDataFromAPI(async () => DashboardAnalisaSales.getAnalisaSalesCall(periode, jabatan, kodeff), (res, i) => handleResponse(res, i, type));
        }
        tabelAnalisa = $('#table-check-data').DataTable({
            "scrollX": true,
        });
    } catch (error) {
        console.log('error brain', error)
    }
}

function handleResponse(item, i, type) {
    var title = ""
    if (type == 'docter') {
        title = item.NamaCust
        document.getElementById("namaGT").value = `${item.NamaGT}`
    } else if (type == 'product') {
        document.getElementById("namaGT").value = `${item.NamaCust}`
        title = item.NamaProduk
    } else if (type == 'outlet') {
        document.getElementById("namaGT").value = `${item.NamaCust}`
        title = item.NamaOutlet
    }
    totalJan += parseFloat(item.Jan);
    totalFeb += parseFloat(item.Feb);
    totalMar += parseFloat(item.Mar);
    totalApr += parseFloat(item.Apr);
    totalMei += parseFloat(item.Mei);
    totalJun += parseFloat(item.Jun);
    totalJul += parseFloat(item.Jul);
    totalAgs += parseFloat(item.Ags);
    totalSep += parseFloat(item.Sep);
    totalOkt += parseFloat(item.Okt);
    totalNov += parseFloat(item.Nov);
    totalDes += parseFloat(item.Des);
    $("#totalJan").html(toIDR(totalJan))
    $('#totalFeb').html(toIDR(totalFeb))
    $('#totalMar').html(toIDR(totalMar))
    $('#totalApr').html(toIDR(totalApr))
    $('#totalMei').html(toIDR(totalMei))
    $('#totalJun').html(toIDR(totalJun))
    $('#totalJul').html(toIDR(totalJul))
    $('#totalAgs').html(toIDR(totalAgs))
    $('#totalSep').html(toIDR(totalSep))
    $('#totalOkt').html(toIDR(totalOkt))
    $('#totalNov').html(toIDR(totalNov))
    $('#totalDes').html(toIDR(totalDes))
    if (i == 0) {
        $('#table-check-data').append(`
                <tfoot>
                    <tr style="background-color:#F76BC0">
                        <th scope="row"></th>
                        <th scope="row">JUMLAH KESELURUHAN :</th>
                        <th scope="row"></th>
                        <th scope="row"></th>
                        <th scope="row" id="totalJan"></th>
                        <th scope="row" id="totalFeb"></th>
                        <th scope="row" id="totalMar"></th>
                        <th scope="row" id="totalApr"></th>
                        <th scope="row" id="totalMei"></th>
                        <th scope="row" id="totalJun"></th>
                        <th scope="row" id="totalJul"></th>
                        <th scope="row" id="totalAgs"></th>
                        <th scope="row" id="totalSep"></th>
                        <th scope="row" id="totalOkt"></th>
                        <th scope="row" id="totalNov"></th>
                        <th scope="row" id="totalDes"></th>
                        <th scope="row"></th>
                    </tr>
            </tfoot>
        `)
    }
    return (
        `<tr style="background-color:#87CEFA">
            <td>${item.Periode}</td>
            <td>${title}</td>
            <td>${item.JabGT}</td>
            <td>${item.KodeArea}</td>
            <td>${item.Jan.toIDR()}</td>
            <td>${item.Feb.toIDR()}</td>
            <td>${item.Mar.toIDR()}</td>
            <td>${item.Apr.toIDR()}</td>
            <td>${item.Mei.toIDR()}</td>
            <td>${item.Jun.toIDR()}</td>
            <td>${item.Jul.toIDR()}</td>
            <td>${item.Ags.toIDR()}</td>
            <td>${item.Sep.toIDR()}</td>
            <td>${item.Okt.toIDR()}</td>
            <td>${item.Nov.toIDR()}</td>
            <td>${item.Des.toIDR()}</td>
            <td style="background-color:#48C9B0">${item.Total.toIDR()}</td>
            <td>
                <button class="btn btn-success btn-sm btn-icon-only" onclick="datadetail('${periodeinput.val()}','${jabataninput.val()}','${item.KodeArea}','','docter')" data-toggle="tooltip" title="Docter" type="button">
                        <i class="material-icons-outlined">face</i>
                </button>
                <button class="btn btn-primary btn-sm btn-icon-only" onclick="datadetail('${periodeinput.val()}','${jabataninput.val()}','${item.KodeArea}','${item.KodeCust}','product')" data-bs-toggle="tooltip" title="Product" type="button">
                    <i class="material-icons-outlined">layers</i>
                </button>
                <button class="btn btn-warning btn-sm btn-icon-only" onclick="datadetail('${periodeinput.val()}','${jabataninput.val()}','${item.KodeArea}','${item.KodeCust}','outlet')" data-bs-toggle="tooltip" title="Outlet" type="button">
                     <i class="material-icons-outlined">store</i>
                </button>
                <button class="btn btn-warning btn-sm btn-icon-only" onclick="datadetail('${periodeinput.val()}','${jabataninput.val()}','${item.KodeArea}','call')" data-bs-toggle="tooltip" title="Outlet" type="button">
                     <i class="material-icons-outlined">settings_phone</i>
                </button>
                <button class="btn btn-danger btn-sm btn-icon-only" data-bs-toggle="tooltip" 
                onclick="datadocter('bawahan')" title="Bahawan" type="button">
                    <i class="material-icons-outlined">arrow_downward</i>
                </button>
            </td>
        </tr>
     `
    );
}