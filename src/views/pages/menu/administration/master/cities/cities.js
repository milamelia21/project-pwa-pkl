$('#page-title').titlePage({
    icon: 'location_city',
    iconColor: ['#84fab0', '#8fd3f4'],
    title: 'City',
    subtitle: 'A financial institution that accepts deposits from the public and creates credit.'
});

$('#page-content').load('src/views/pages/menu/administration/master/cities/cities-table.html')