// initial state
$('input').autocompleteOff();
if (citiesSelected) {
    $('#name-cities').val(citiesSelected.name);
    $('#id-cities').val(citiesSelected.id);
    document.getElementById('id-cities').readOnly = true;
}

// event
$('#cities-create').submit(async function (e) {
    e.preventDefault();
    var isValid = true;
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
        e.stopPropagation();
        isValid = false;
    }
    form.classList.add('was-validated');
    if (isValid) {
        var response
        var id = form['id-cities'].value
        var name = form['name-cities'].value
        if (citiesSelected == null) {
            response = await master.createCities(id, name)
        } else {
            response = await master.updateCities(citiesSelected.id, name)
        }

        if (response.success == true) {
            $('#back').trigger('click');
        }
    }
});
$('#form-cancel, #back').click(function (e) {
    e.preventDefault();
    $('#page-content').load('src/views/pages/menu/administration/master/cities/cities-table.html')
});