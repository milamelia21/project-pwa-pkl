var citiesSelected;
var mainTable;
var pageNumber;
async function setTableCities() {
    await $('#table-cities').tableSetDataFromAPI(async () => master.getCities(), (item) => {
    return/*html*/` <tr>
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                        <td>${item.created_at.dateFormat()}</td>
                        <td>${item.updated_at.dateFormat()}</td>
                        <td>
                            <button class="btn btn-warning btn-sm btn-icon-only edit-button" onclick="editFunction(this)" data-toggle="tooltip" title="Edit" type="button" data-json='${JSON.stringify(item)}''>
                                <i class="material-icons-outlined">edit</i>
                            </button>
                            <button class="btn btn-danger btn-sm btn-icon-only delete-button" onclick="deleteFunction(this)" data-toggle="tooltip" title="Delete" type="button" data-json='${JSON.stringify(item)}''>
                                <i class="material-icons-outlined">delete</i>
                            </button>
                        </td>
                    </tr>`
    })

    mainTable = $('#table-cities').datatable();
    mainTable.page(pageNumber).draw(false);
}
setTableCities();

//events
$('#cities-create').click(function (e) {
    citiesSelected = null;
    $('#page-content').load('src/views/pages/menu/administration/master/cities/cities-create.html')
});

function editFunction(e) {
    citiesSelected = JSON.parse(e.dataset.json);
    $('#page-content').load('src/views/pages/menu/administration/master/cities/cities-create.html')
};

async function deleteFunction(e) {
    if (await MessageBox.confirm()) {
        citiesSelected = JSON.parse(e.dataset.json);
        var result = await Cities.deleteCisties(
            citiesSelected.id,
        );
        if (result)
            pageNumber = (mainTable.page.info()).page;
            $('#page-content').load('src/views/pages/menu/administration/master/cities/cities-table.html')
    }
}