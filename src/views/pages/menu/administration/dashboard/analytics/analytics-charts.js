$('select').comboboxSearch();

// ANCHOR Responsive Border
function windowWidth() {
    if ($(window).width() < 575) {
        $('.main-button').addClass('border');
        $('.main-button').addClass('border-info');
        $('.main-button').addClass('my-1');
        $('.main-button').addClass('py-1');
        $('.main-button').addClass('rounded');
        $('.main-button').removeClass('border-left');
    } else {
        $('.main-button').removeClass('border');
        $('.main-button').removeClass('border-info');
        $('.main-button').removeClass('my-1');
        $('.main-button').removeClass('py-1');
        $('.main-button').removeClass('rounded');
        $('.main-button').addClass('border-left');
    }
}

$(window).on('resize', function () {
    if ($(window).width() < 575) {
        $('.main-button').addClass('border');
        $('.main-button').addClass('border-info');
        $('.main-button').addClass('my-1');
        $('.main-button').addClass('py-1');
        $('.main-button').addClass('rounded');
        $('.main-button').removeClass('border-left');
    } else {
        $('.main-button').removeClass('border');
        $('.main-button').removeClass('border-info');
        $('.main-button').removeClass('my-1');
        $('.main-button').removeClass('py-1');
        $('.main-button').removeClass('rounded');
        $('.main-button').addClass('border-left');
    }
})
// ANCHOR Move Array
function arrayMove(arr, old_index, new_index) {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
};

// ANCHOR Clear Space
function clearSpace(kode) {
    var clear = kode.replace(" ", "")
    return clear
}

// ANCHOR Nominal
function nominal(nilai) {
    if (nilai >= 1000000000) {
        var hasil = (nilai / 1000000000).toFixed(2) + " M"
        return hasil
    } else if (nilai >= 1000000) {
        var hasil = (nilai / 1000000).toFixed(2) + " JT"
        return hasil
    } else if (nilai >= 1000) {
        hasil = (nilai / 1000).toFixed(2) + " RB"
        return hasil
    } else if (nilai <= 0) {
        return 0
    } else {
        return nilai
    }
}

// ANCHOR Show Loading
function showLoading() {
    Swal.fire({
        title: 'PLEASE WAIT',
        text: 'processing your request',
        allowOutsideClick: false,
        onBeforeOpen: () => {
            Swal.showLoading()
        },
    });
}

// ANCHOR End Loading
function endLoading() {
    Swal.close();
}

// ANCHOR Get Month
function getMonth(periode) {
    if (periode == "01") {
        return "Jan"
    } else if (periode == "02") {
        return "Feb"
    } else if (periode == "03") {
        return "Mar"
    } else if (periode == "04") {
        return "Apr"
    } else if (periode == "05") {
        return "May"
    } else if (periode == "06") {
        return "Jun"
    } else if (periode == "07") {
        return "Jul"
    } else if (periode == "08") {
        return "Aug"
    } else if (periode == "09") {
        return "Sep"
    } else if (periode == "10") {
        return "Oct"
    } else if (periode == "11") {
        return "Nov"
    } else if (periode == "12") {
        return "Dec"
    }
}

// ANCHOR Convert Month
function convertMonth(data, praData, praYTD, praEstimasi) {
    for (i = 0; i < 12; i++) {
        if (data.data[i] != undefined) {
            if (data.data[i].Periode.substring(0, 4) == new Date().getFullYear().toString()) {
                if (data.data[i].Periode.substring(4, 6) == "01") {
                    praData[0] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "02") {
                    praData[1] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "03") {
                    praData[2] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "04") {
                    praData[3] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "05") {
                    praData[4] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "06") {
                    praData[5] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "07") {
                    praData[6] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "08") {
                    praData[7] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "09") {
                    praData[8] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "10") {
                    praData[9] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "11") {
                    praData[10] = data.data[i].Nilai
                } else if (data.data[i].Periode.substring(4, 6) == "12") {
                    praData[11] = data.data[i].Nilai
                }
            }
        }
        if (data.ytd[i] != undefined) {
            if (data.ytd[i].Periode.substring(0, 4) == ((new Date().getFullYear()) - 1).toString()) {
                if (data.ytd[i].Periode.substring(4, 6) == "01") {
                    praYTD[0] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "02") {
                    praYTD[1] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "03") {
                    praYTD[2] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "04") {
                    praYTD[3] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "05") {
                    praYTD[4] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "06") {
                    praYTD[5] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "07") {
                    praYTD[6] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "08") {
                    praYTD[7] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "09") {
                    praYTD[8] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "10") {
                    praYTD[9] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "11") {
                    praYTD[10] = data.ytd[i].Nilai
                } else if (data.ytd[i].Periode.substring(4, 6) == "12") {
                    praYTD[11] = data.ytd[i].Nilai
                }
            }
        }
        if (data.estimasi[i] != undefined) {
            if (data.estimasi[i].Periode.substring(0, 4) == new Date().getFullYear().toString()) {
                if (data.estimasi[i].Periode.substring(4, 6) == "01") {
                    praEstimasi[0] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "02") {
                    praEstimasi[1] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "03") {
                    praEstimasi[2] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "04") {
                    praEstimasi[3] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "05") {
                    praEstimasi[4] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "06") {
                    praEstimasi[5] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "07") {
                    praEstimasi[6] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "08") {
                    praEstimasi[7] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "09") {
                    praEstimasi[8] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "10") {
                    praEstimasi[9] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "11") {
                    praEstimasi[10] = data.estimasi[i].Nilai
                } else if (data.estimasi[i].Periode.substring(4, 6) == "12") {
                    praEstimasi[11] = data.estimasi[i].Nilai
                }
            }
        }
    }
}

// ANCHOR Chart Option
var chartOption = {
    "hover": {
        "animationDuration": 0
    },
    "animation": {
        "duration": 1,
        "onComplete": function () {
            var chartInstance = this.chart,
                ctx = chartInstance.ctx;

            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';

            this.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.controller.getDatasetMeta(i);
                meta.data.forEach(function (bar, index) {
                    var data = dataset.data[index];
                    var nilai = nominal(data)
                    ctx.font = "14px Arial";
                    ctx.fillText(nilai, bar._model.x, bar._model.y - 5);
                });
            });
        }
    },
    // tooltips: {
    //     yAlign: 'top'
    // },
    maintainAspectRatio: false,
    onClick: function (evt) {
        var element = ctx.getElementAtEvent(evt);
        if (element.length > 0) {
            var ind = element[0]._index;
            if (confirm('Do you want to remove this point?')) {
                data.datasets[0].data.splice(ind, 1);
                data.labels.splice(ind, 1);
                myLineChart.update(data);
            }
        }
    }
};

// ANCHOR Statistic Text & Image
function textStatistic(flag) {
    if (flag == "UP") {
        return `text-success`
    } else if (flag == "SAME") {
        return `text-warning`
    } else {
        return `text-danger`
    }
}

function imageStatistic(flag) {
    if (flag == "UP") {
        return `src/assets/image/growup.svg`
    } else if (flag == "SAME") {
        return `src/assets/image/notgrow.svg`
    } else {
        return `src/assets/image/growdown.svg`
    }
}

// ANCHOR Get Periode Pareto
async function getParetoN(ev) {
    if (ev.target.dataItem.dataContext != undefined) {
        showLoading()
        var periode = ev.target.dataItem.dataContext["valuePeriode"],
            menu = ev.target.dataItem.dataContext["menu"],
            kode = ev.target.dataItem.dataContext["kode"],
            jenis1 = ev.target.dataItem.dataContext["jenis1"],
            jenis2 = ev.target.dataItem.dataContext["jenis2"],
            jenis3 = ev.target.dataItem.dataContext["jenis3"],
            pareto1 = ev.target.dataItem.dataContext["pareto1"],
            pareto2 = ev.target.dataItem.dataContext["pareto2"],
            pareto3 = ev.target.dataItem.dataContext["pareto3"],
            data
        if (menu == "CUSTOMER") {
            if (jenis1 == "FF") {
                data = await DashboardAPI.getCustomerChart(kode, "CALL", periode)
            } else {
                data = await DashboardAPI.getCustomerChart(kode, jenis1, periode)
            }
        } else if (menu == "OUTLET") {
            if (jenis1 == "FF") {
                data = await DashboardAPI.getOutletChart(kode, "CALL", periode)
            } else {
                data = await DashboardAPI.getOutletChart(kode, jenis1, periode)
            }
        } else if (menu == "PRODUK") {
            data = await DashboardAPI.getProductChart(kode, jenis1, periode)
        } else if (menu == "FF") {
            data = await DashboardAPI.getFFChart(kode, jenis1, periode)
        }
        getPareto(jenis1, data.pareto1, pareto1, pareto2, pareto3, kode, periode, menu)
        getPareto(jenis2, data.pareto2, pareto2)
        getPareto(jenis3, data.pareto3, pareto3)
        endLoading()
    }
}

async function getParetoN_1(ev) {
    if (ev.target.dataItem.dataContext != undefined) {
        showLoading()
        var periode = ev.target.dataItem.dataContext["ytdPeriode"],
            menu = ev.target.dataItem.dataContext["menu"],
            kode = ev.target.dataItem.dataContext["kode"],
            jenis1 = ev.target.dataItem.dataContext["jenis1"],
            jenis2 = ev.target.dataItem.dataContext["jenis2"],
            jenis3 = ev.target.dataItem.dataContext["jenis3"],
            pareto1 = ev.target.dataItem.dataContext["pareto1"],
            pareto2 = ev.target.dataItem.dataContext["pareto2"],
            pareto3 = ev.target.dataItem.dataContext["pareto3"],
            data
        if (menu == "CUSTOMER") {
            if (jenis1 == "FF") {
                data = await DashboardAPI.getCustomerChart(kode, "CALL", periode)
            } else {
                data = await DashboardAPI.getCustomerChart(kode, jenis1, periode)
            }
        } else if (menu == "OUTLET") {
            if (jenis1 == "FF") {
                data = await DashboardAPI.getOutletChart(kode, "CALL", periode)
            } else {
                data = await DashboardAPI.getOutletChart(kode, jenis1, periode)
            }
        } else if (menu == "PRODUK") {
            data = await DashboardAPI.getProductChart(kode, jenis1, periode)
        } else if (menu == "FF") {
            data = await DashboardAPI.getFFChart(kode, jenis1, periode)
        }
        getPareto(jenis1, data.pareto1, pareto1, pareto2, pareto3, kode, periode, menu)
        getPareto(jenis2, data.pareto2, pareto2)
        getPareto(jenis3, data.pareto3, pareto3)
        endLoading()
    }
}

// ANCHOR Get Pareto
function getPareto(jenis, data, element, element2, element3, kode, periode, menu, nip) {
    if (data != undefined) {
        var head
        if (element2 == undefined && kode == undefined && periode == undefined) {
            head = `<div class="list-group-item list-group-item-action flex-column align-items-start bg-info text-white border-info"
                style="padding: .25rem 1.25rem">
                <div class="d-flex w-100 justify-content-between">
                    <strong>` + jenis + ` (` + data.length + `)</strong>
                    <small>PARETO</small>
                </div>
            </div>`
        } else {
            head = `<div class="list-group-item list-group-item-action flex-column align-items-start bg-info text-white border-info"
                style="padding: .25rem 1.25rem">
                <div class="d-flex w-100 justify-content-between">
                    <strong>` + jenis + ` (` + data.length + `)</strong>
                    <small>VALUE</small>
                </div>
            </div>`
        }

        $(element).empty()
        $(element2).empty()
        $(element).append(head)
        $.each(data, function (index, value) {
            var html = `<div class="list-group-item list-group-item-action flex-column align-items-start" style="padding: .25rem 1.25rem" onclick="getDetailPareto(this,'` + menu + `', '` + value.Nama + `','` + kode + `','` + jenis + `','` + periode + `','` + value.Kode + `','` + element2 + `','` + element3 + `','` + nip + `')">
                <div class="d-flex w-100 justify-content-between">
                    <small>` + value.Nama + `</small>
                    <small class="text-info">` + (value.Nilai).replace(".00", "") + `</small>
                </div>
            </div>`
            $(element).append(html)
        });
    }
}

// ANCHOR Get Detail Pareto
async function getDetailPareto(element, menu, nama, kode, jenis, periode, kodeParam, element2, element3, nip) {
    showLoading()
    var data
    if (menu == "CUSTOMER") {
        if (jenis == "FF") {
            data = await DashboardAPI.getCustomerChart(kode, "CALL", periode, "", "", kodeParam)
        } else if (jenis == "TANGGAL") {
            data = await DashboardAPI.getCustomerChart(kode, "CALL", periode, "", "", nip, kodeParam)
        } else {
            data = await DashboardAPI.getCustomerChart(kode, jenis, periode, kodeParam, kodeParam)
        }
    } else if (menu == "OUTLET") {
        if (jenis == "FF") {
            data = await DashboardAPI.getOutletChart(kode, "CALL", periode, "", "", kodeParam)
        } else if (jenis == "TANGGAL") {
            data = await DashboardAPI.getOutletChart(kode, "CALL", periode, "", "", nip, kodeParam)
        } else {
            data = await DashboardAPI.getOutletChart(kode, jenis, periode, kodeParam, kodeParam)
        }
    } else if (menu == "PRODUK") {
        data = await DashboardAPI.getProductChart(kode, jenis, periode, kodeParam, kodeParam, kodeParam)
    } else if (menu == "FF") {
        data = await DashboardAPI.getFFChart(kode, jenis, periode, kodeParam, kodeParam, kodeParam)
    } else {
        endLoading()
    }
    if (data != undefined) {
        $(".list-group-item").removeClass("active")
        $(element).addClass("active")
        if ((menu == "CUSTOMER" || menu == "OUTLET") && jenis == "FF") {
            getPareto("TANGGAL", data.pareto2, element2, element3, element3, kode, periode, menu, kodeParam)
        } else if ((menu == "CUSTOMER" || menu == "OUTLET") && jenis == "TANGGAL") {
            getPareto("PRODUK", data.pareto3, element2, element3, element3, kode, periode)
        } else {
            getPareto(nama, data.pareto2, element2, undefined, undefined, kode, periode)
            if ((menu == "PRODUK" && jenis == "FF") || (menu == "FF" && jenis == "PRODUK")) {
                getPareto(nama, data.pareto3, element3, undefined, undefined, kode, periode)
            }
        }
    }
    endLoading()
}

// ANCHOR Dynamic Colors
var dynamicColors = function () {
    var r = Math.floor(Math.random() * 250) + 50;
    var g = Math.floor(Math.random() * 200) + 50;
    var b = Math.floor(Math.random() * 250) + 100;
    return "rgb(" + r + "," + g + "," + b + ")";
};

// NOTE Search
function inputSearch(id, type) {
    var search = $('#' + id).val()
    if (type == "Customer") {
        setCustomerChart(search)
    } else if (type == "Outlet") {
        setOutletChart(search)
    } else if (type == "Product") {
        setProductChart(search)
    } else if (type == "FF") {
        setFFChart(search)
    }
}

// NOTE Group Filter

var filterType

function setType(type) {
    filterType = type
}

function groupFilter(ev) {
    var label = ev.target.dataItem.dataContext.label;
    var series = ev.target.dataItem.component;
    series.slices.each(function (item) {
        if (item != ev.target) {
            item.isActive = false;
        } else if (item.isActive == true) {
            if (filterType == "Customer") {
                setCustomerChart(undefined, label)
            } else if (filterType == "Outlet") {
                setOutletChart(undefined, label)
            } else if (filterType == "Product") {
                setProductChart(undefined, label)
            } else if (filterType == "FF") {
                setFFChart(undefined, label)
            }
        } else if (item.isActive == false) {
            if (filterType == "Customer") {
                setCustomerChart()
            } else if (filterType == "Outlet") {
                setOutletChart()
            } else if (filterType == "Product") {
                setProductChart()
            } else if (filterType == "FF") {
                setFFChart()
            }
        }
    })
}

// NOTE Group Sort
$("#CustomerSort").change(function (e) {
    var result = $('.CustomerFilterName').sort(function (a, b) {
        var contentA = parseInt($(a).data(e.currentTarget.value));
        var contentB = parseInt($(b).data(e.currentTarget.value));
        return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
    });
    $('#customerMenu').html(result);
});

$("#OutletSort").change(function (e) {
    var result = $('.OutletFilterName').sort(function (a, b) {
        var contentA = parseInt($(a).data(e.currentTarget.value));
        var contentB = parseInt($(b).data(e.currentTarget.value));
        return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
    });
    $('#outletMenu').html(result);
});

$("#ProductSort").change(function (e) {
    var result = $('.ProductFilterName').sort(function (a, b) {
        var contentA = parseInt($(a).data(e.currentTarget.value));
        var contentB = parseInt($(b).data(e.currentTarget.value));
        return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
    });
    $('#productMenu').html(result);
});

$("#FFSort").change(function (e) {
    var result = $('.FFFilterName').sort(function (a, b) {
        var contentA = parseInt($(a).data(e.currentTarget.value));
        var contentB = parseInt($(b).data(e.currentTarget.value));
        return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
    });
    $('#FFMenu').html(result);
});

// NOTE Pie Chart Customer
async function setCustomerPieChart() {
    var data = await DashboardAPI.getCustomerPieChart()
    if (data != undefined) {
        var chartData = []
        for (i = 0; i < data.data.length; i++) {
            chartData.push({
                label: data.data[i].Keterangan,
                value: data.data[i].Nilai
            })
        }
        for (i = 0; i < chartData.length; i++) {
            if (chartData[i].label == "Lain-lain") {
                arrayMove(chartData, i, chartData.length - 1)
            }
        }
        am4core.ready(function () {
            am4core.useTheme(am4themes_material);
            var chart = am4core.create("CustomerPieChart", am4charts.PieChart);
            chart.hiddenState.properties.opacity = 0;
            chart.logo.height = -15
            chart.data = chartData;
            chart.radius = am4core.percent(70);
            chart.innerRadius = am4core.percent(40);
            chart.startAngle = 180;
            chart.endAngle = 360;


            var series = chart.series.push(new am4charts.PieSeries());
            series.dataFields.value = "value";
            series.dataFields.category = "label";
            series.labels.template.maxWidth = 130;
            series.labels.template.truncate = true;


            series.showTooltipOn = "always";

            series.slices.template.cornerRadius = 10;
            series.slices.template.innerCornerRadius = 7;
            series.slices.template.draggable = false;
            series.slices.template.inert = true;
            series.alignLabels = false;

            series.hiddenState.properties.startAngle = 90;
            series.hiddenState.properties.endAngle = 90;

            series.slices.template.events.on("hit", groupFilter, setType("Customer"));

            var legend = chart.legend = new am4charts.Legend();
            legend.position = "right";
            legend.maxHeight = 380;
            legend.scrollable = true;
            if ($(window).width() < 800) {
                legend.disabled = true
            } else {
                legend.disabled = false
            }
            $(window).on('resize', function () {
                if ($(window).width() < 800) {
                    legend.disabled = true
                } else {
                    legend.disabled = false
                }
            })
        });
    }
}

// NOTE Pie Chart Outlet
async function setOutletPieChart() {
    var data = await DashboardAPI.getOutletPieChart()
    if (data != undefined) {
        var chartData = []
        for (i = 0; i < data.data.length; i++) {
            chartData.push({
                label: data.data[i].Keterangan,
                value: data.data[i].Nilai
            })
        }
        for (i = 0; i < chartData.length; i++) {
            if (chartData[i].label == "Lain-lain") {
                arrayMove(chartData, i, chartData.length - 1)
            }
        }
        am4core.ready(function () {
            am4core.useTheme(am4themes_material);
            var chart = am4core.create("OutletPieChart", am4charts.PieChart);
            chart.hiddenState.properties.opacity = 0;
            chart.logo.height = -15
            chart.data = chartData;
            chart.radius = am4core.percent(70);
            chart.innerRadius = am4core.percent(40);
            chart.startAngle = 180;
            chart.endAngle = 360;


            var series = chart.series.push(new am4charts.PieSeries());
            series.dataFields.value = "value";
            series.dataFields.category = "label";
            series.labels.template.maxWidth = 130;
            series.labels.template.truncate = true;

            series.showTooltipOn = "always";

            series.slices.template.cornerRadius = 10;
            series.slices.template.innerCornerRadius = 7;
            series.slices.template.draggable = false;
            series.slices.template.inert = true;
            series.alignLabels = false;

            series.hiddenState.properties.startAngle = 90;
            series.hiddenState.properties.endAngle = 90;

            series.slices.template.events.on("hit", groupFilter, setType("Outlet"));

            var legend = chart.legend = new am4charts.Legend();
            legend.position = "right";
            legend.maxHeight = 380;
            legend.scrollable = true;
            if ($(window).width() < 800) {
                legend.disabled = true
            } else {
                legend.disabled = false
            }
            $(window).on('resize', function () {
                if ($(window).width() < 800) {
                    legend.disabled = true
                } else {
                    legend.disabled = false
                }
            })
        });
    }
}

// NOTE Pie Chart Product
async function setProductPieChart() {
    var data = await DashboardAPI.getProductPieChart()
    if (data != undefined) {
        var chartData = []
        for (i = 0; i < data.data.length; i++) {
            chartData.push({
                label: data.data[i].Keterangan,
                value: data.data[i].Nilai
            })
        }
        for (i = 0; i < chartData.length; i++) {
            if (chartData[i].label == "Lain-lain") {
                arrayMove(chartData, i, chartData.length - 1)
            }
        }
        am4core.ready(function () {
            am4core.useTheme(am4themes_material);
            var chart = am4core.create("ProductPieChart", am4charts.PieChart);
            chart.hiddenState.properties.opacity = 0;
            chart.logo.height = -15
            chart.data = chartData;
            chart.radius = am4core.percent(70);
            chart.innerRadius = am4core.percent(40);
            chart.startAngle = 180;
            chart.endAngle = 360;


            var series = chart.series.push(new am4charts.PieSeries());
            series.dataFields.value = "value";
            series.dataFields.category = "label";
            series.labels.template.maxWidth = 130;
            series.labels.template.truncate = true;

            series.showTooltipOn = "always";

            series.slices.template.cornerRadius = 10;
            series.slices.template.innerCornerRadius = 7;
            series.slices.template.draggable = false;
            series.slices.template.inert = true;
            series.alignLabels = false;

            series.hiddenState.properties.startAngle = 90;
            series.hiddenState.properties.endAngle = 90;

            var legend = chart.legend = new am4charts.Legend();
            legend.position = "right";
            legend.maxHeight = 380;
            legend.scrollable = true;
            if ($(window).width() < 800) {
                legend.disabled = true
            } else {
                legend.disabled = false
            }
            $(window).on('resize', function () {
                if ($(window).width() < 800) {
                    legend.disabled = true
                } else {
                    legend.disabled = false
                }
            })
        });
    }
}

// NOTE Pie Chart FF
async function setFFPieChart() {
    var data = await DashboardAPI.getFFPieChart()
    if (data != undefined) {
        var chartData = []
        for (i = 0; i < data.data.length; i++) {
            chartData.push({
                label: data.data[i].Keterangan,
                value: data.data[i].Nilai
            })
        }
        for (i = 0; i < chartData.length; i++) {
            if (chartData[i].label == "Lain-lain") {
                arrayMove(chartData, i, chartData.length - 1)
            }
        }
        am4core.ready(function () {
            am4core.useTheme(am4themes_material);
            var chart = am4core.create("FFPieChart", am4charts.PieChart);
            chart.hiddenState.properties.opacity = 0;
            chart.logo.height = -15
            chart.data = chartData;
            chart.radius = am4core.percent(70);
            chart.innerRadius = am4core.percent(40);
            chart.startAngle = 180;
            chart.endAngle = 360;


            var series = chart.series.push(new am4charts.PieSeries());
            series.dataFields.value = "value";
            series.dataFields.category = "label";
            series.labels.template.maxWidth = 130;
            series.labels.template.truncate = true;

            series.showTooltipOn = "always";

            series.slices.template.cornerRadius = 10;
            series.slices.template.innerCornerRadius = 7;
            series.slices.template.draggable = false;
            series.slices.template.inert = true;
            series.alignLabels = false;

            series.hiddenState.properties.startAngle = 90;
            series.hiddenState.properties.endAngle = 90;

            series.slices.template.events.on("hit", groupFilter, setType("FF"));

            var legend = chart.legend = new am4charts.Legend();
            legend.position = "right";
            legend.maxHeight = 380;
            legend.scrollable = true;
            if ($(window).width() < 800) {
                legend.disabled = true
            } else {
                legend.disabled = false
            }
            $(window).on('resize', function () {
                if ($(window).width() < 800) {
                    legend.disabled = true
                } else {
                    legend.disabled = false
                }
            })
        });
    }
}

// TODO Customer - Sales CN
async function setCustomerChartSalesCN(kodeCust) {
    kodeCust = clearSpace(kodeCust)
    if ($('#CustomerChartSalesCN_' + kodeCust).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getCustomerChart(kodeCust, "DEFAULT")
        $("#CustomerLoadingSalesCN_" + kodeCust).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praEstimasi = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD, praEstimasi)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "estimasiPeriode": new Date().getFullYear().toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "estimasi": praEstimasi[i],
                "estimasiNominal": nominal(praEstimasi[i]),
                "menu": "CUSTOMER",
                "kode": kodeCust,
                "jenis1": "OUTLET",
                "jenis2": "PRODUK",
                "jenis3": "CALL",
                "pareto1": "#CustomerSalesCN_" + kodeCust + "_Pareto1",
                "pareto2": "#CustomerSalesCN_" + kodeCust + "_Pareto2",
                "pareto3": "#CustomerSalesCN_" + kodeCust + "_Pareto3",
            }
        }
        var pareto1 = '#CustomerSalesCN_' + kodeCust + '_Pareto1',
            pareto2 = '#CustomerSalesCN_' + kodeCust + '_Pareto2',
            pareto3 = '#CustomerSalesCN_' + kodeCust + '_Pareto3'
        getPareto('OUTLET', data.pareto1, pareto1)
        getPareto('PRODUK', data.pareto2, pareto2)
        getPareto('CALL', data.pareto3, pareto3)

        var chart = am4core.create('CustomerChartSalesCN_' + kodeCust, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();
        valueAxis.numberFormatter.numberFormat = '#a';
        valueAxis.numberFormatter.bigNumberPrefixes = [{
                "number": 1e+3,
                "suffix": " RB"
            },
            {
                "number": 1e+6,
                "suffix": " JT"
            },
            {
                "number": 1e+9,
                "suffix": " M"
            }
        ];

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        var series3 = chart.series.push(new am4charts.LineSeries());
        series3.dataFields.valueY = "estimasi";
        series3.dataFields.categoryX = "month";
        series3.name = "Estimasi " + new Date().getFullYear().toString();
        series3.strokeWidth = 1;
        series3.tensionX = 0.7;
        series3.bullets.push(new am4charts.CircleBullet());
        series3.tooltipText = "Estimasi : [bold]{estimasiNominal}[/]";
        series3.stroke = am4core.color("rgba(0, 170, 40, 1)")
        series3.fill = am4core.color("rgba(0, 170, 40, 1)")
        series3.strokeDasharray = "2";
        series3.tooltip.label.interactionsEnabled = true;
        series3.tooltip.keepTargetHover = true;
        series3.tooltip.pointerOrientation = "vertical";
        series3.tooltip.label.textAlign = "middle";
        series3.tooltip.label.textValign = "middle";

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#CustomerChartSalesCN_' + kodeCust).removeClass("unloaded")
    }
}

// TODO Customer - Outlet
async function setCustomerChartOutlet(kodeCust) {
    kodeCust = clearSpace(kodeCust)
    if ($('#CustomerChartOutlet_' + kodeCust).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getCustomerChart(kodeCust, "OUTLET")
        $("#CustomerLoadingOutlet_" + kodeCust).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "CUSTOMER",
                "kode": kodeCust,
                "jenis1": "OUTLET",
                "jenis2": "PRODUK",
                "jenis3": "CALL",
                "pareto1": "#CustomerOutlet_" + kodeCust + "_Pareto1",
                "pareto2": "#CustomerOutlet_" + kodeCust + "_Pareto2",
                "pareto3": "#CustomerOutlet_" + kodeCust + "_Pareto3",
            }
        }

        var chart = am4core.create('CustomerChartOutlet_' + kodeCust, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#CustomerChartOutlet_' + kodeCust).removeClass("unloaded")
    }
}

// TODO Customer - Product
async function setCustomerChartProduct(kodeCust) {
    kodeCust = clearSpace(kodeCust)
    if ($('#CustomerChartProduct_' + kodeCust).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getCustomerChart(kodeCust, "PRODUK")
        $("#CustomerLoadingProduct_" + kodeCust).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "CUSTOMER",
                "kode": kodeCust,
                "jenis1": "PRODUK",
                "jenis2": "OUTLET",
                "jenis3": "CALL",
                "pareto1": "#CustomerProduct_" + kodeCust + "_Pareto1",
                "pareto2": "#CustomerProduct_" + kodeCust + "_Pareto2",
                "pareto3": "#CustomerProduct_" + kodeCust + "_Pareto3",
            }
        }

        var chart = am4core.create('CustomerChartProduct_' + kodeCust, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = (new Date().getFullYear()) - 1 + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#CustomerChartProduct_' + kodeCust).removeClass("unloaded")
    }
}

// TODO Customer - Call
async function setCustomerChartFF(kodeCust) {
    kodeCust = clearSpace(kodeCust)
    if ($('#CustomerChartFF_' + kodeCust).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getCustomerChart(kodeCust, "CALL")
        $("#CustomerLoadingFF_" + kodeCust).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "CUSTOMER",
                "kode": kodeCust,
                "jenis1": "FF",
                "jenis2": "TANGGAL",
                "jenis3": "CALL",
                "pareto1": "#CustomerFF_" + kodeCust + "_Pareto1",
                "pareto2": "#CustomerFF_" + kodeCust + "_Pareto2",
                "pareto3": "#CustomerFF_" + kodeCust + "_Pareto3",
            }
        }

        var chart = am4core.create('CustomerChartFF_' + kodeCust, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = (new Date().getFullYear()) - 1 + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#CustomerChartFF_' + kodeCust).removeClass("unloaded")
    }
}

// REVIEW Customer
async function setCustomerChart(search, filter) {
    showLoading()
    await $('#customerMenu').dashboardSetDataFromAPI(DashboardAPI.getCustomer, search, filter, item => {
        var Nilai = nominal(item.Nilai)
        var textSales = textStatistic(item.FlagCN),
            textOutlet = textStatistic(item.FlagOutlet),
            textProduct = textStatistic(item.FlagProd),
            textCall = textStatistic(item.FlagCall)
        var imageSales = imageStatistic(item.FlagCN),
            imageOutlet = imageStatistic(item.FlagOutlet),
            imageProduct = imageStatistic(item.FlagProd),
            imageCall = imageStatistic(item.FlagCall)
        html = `<div class="Filter CustomerFilterName" data-filter="${item.Keterangan}" data-filterName="${item.NamaCust}" data-1="${item.Nilai}" data-2="${item.JmlOutlet}" data-3="${item.JmlProduk}" data-4="${item.JmlCall}">
            <button class="list-group-item list-group-item-action mt-2 border-dark" id="${item.KodeCust}">
                <div class="row rounded">
                    <div class="col-sm-4 media" data-toggle="collapse" data-target="#CustomerSalesCN_${item.KodeCust}" onclick="setCustomerChartSalesCN('${item.KodeCust}')"><img src="src/assets/image/doctor-placeholder.jpg"
                        class="img-fluid rounded-circle mr-2" style="width: 40px;">
                        <div class="">
                            <strong>${item.NamaCust}</strong>
                            <p class="font-weight-normal text-info">${item.SPC}</p>
                        </div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#CustomerSalesCN_${item.KodeCust}" onclick="setCustomerChartSalesCN('${item.KodeCust}')">
                        <h4 class="text-center ${textSales}"><img src="${imageSales}"
                        class="img-fluid mr-2" style="width: 14px;">${Nilai}
                        </h4>
                        <div class="small text-center text-info">SALES</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#CustomerOutlet_${item.KodeCust}" onclick="setCustomerChartOutlet('${item.KodeCust}')">
                    <h4 class="text-center ${textOutlet}"><img src="${imageOutlet}"
                        class="img-fluid mr-2" style="width: 14px;">${item.JmlOutlet}
                    </h4>
                    <div class="small text-center text-info">OUTLET</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#CustomerProduct_${item.KodeCust}" onclick="setCustomerChartProduct('${item.KodeCust}')">
                    <h4 class="text-center ${textProduct}"><img src="${imageProduct}"
                        class="img-fluid mr-2" style="width: 14px;">${item.JmlProduk}
                    </h4>
                    <div class="small text-center text-info">PRODUK</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#CustomerFF_${item.KodeCust}" onclick="setCustomerChartFF('${item.KodeCust}')">
                    <h4 class="text-center ${textCall}"><img src="${imageCall}"
                        class="img-fluid mr-2" style="width: 14px;">${item.JmlCall}
                    </h4>
                    <div class="small text-center text-info">CALL</div>
                    </div>
                </div>
            </button>
            <div id="mainChart_${item.KodeCust}" data-filter="${item.Keterangan}" data-filterName="${item.NamaCust}">
                <div id="CustomerSalesCN_${item.KodeCust}" data-parent="#mainChart_${item.KodeCust}" class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info"><h5><strong>SALES</strong></h5></div>
                        <div class="col-12 text-center" id="CustomerLoadingSalesCN_${item.KodeCust}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;"id="CustomerChartSalesCN_${item.KodeCust}" class="unloaded"></div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="CustomerSalesCN_${item.KodeCust}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="CustomerSalesCN_${item.KodeCust}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="CustomerSalesCN_${item.KodeCust}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="CustomerOutlet_${item.KodeCust}" data-parent="#mainChart_${item.KodeCust}" class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info"><h5><strong>OUTLET</strong></h5></div>
                        <div class="col-12 text-center" id="CustomerLoadingOutlet_${item.KodeCust}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;"id="CustomerChartOutlet_${item.KodeCust}" class="unloaded"></div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="CustomerOutlet_${item.KodeCust}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="CustomerOutlet_${item.KodeCust}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-12 border-top border-info">
                            <div class="list-group pt-1 mt-2" id="CustomerOutlet_${item.KodeCust}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="CustomerProduct_${item.KodeCust}" data-parent="#mainChart_${item.KodeCust}" class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info"><h5><strong>PRODUK</strong></h5></div>
                        <div class="col-12 text-center" id="CustomerLoadingProduct_${item.KodeCust}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;"id="CustomerChartProduct_${item.KodeCust}" class="unloaded"></div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="CustomerProduct_${item.KodeCust}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="CustomerProduct_${item.KodeCust}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-12 border-top border-info">
                            <div class="list-group pt-1 mt-2" id="CustomerProduct_${item.KodeCust}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="CustomerFF_${item.KodeCust}" data-parent="#mainChart_${item.KodeCust}" class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info"><h5><strong>CALL</strong></h5></div>
                        <div class="col-12 text-center" id="CustomerLoadingFF_${item.KodeCust}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;"id="CustomerChartFF_${item.KodeCust}" class="unloaded"></div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="CustomerFF_${item.KodeCust}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="CustomerFF_${item.KodeCust}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="CustomerFF_${item.KodeCust}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`
        return html;
    })
    $('#customerHeader').removeClass("unloaded")
    endLoading()
    windowWidth()
    if (search != undefined) {
        $('#customerFilter').html('<b>Filter by name :</b> "' + search + '" (Sales > 0)')
    } else if (filter != undefined) {
        $('#customerFilter').html('<b>Filter by category :</b> "' + filter + '" (Sales > 0)')
    } else {
        $('#customerFilter').html('<b>Filter by :</b> Top 10 Sales')
    }
}

// TODO Outlet - Sales CN
async function setOutletChartSalesCN(kodeOutlet) {
    kodeOutlet = clearSpace(kodeOutlet)
    if ($('#OutletChartSalesCN_' + kodeOutlet).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getOutletChart(kodeOutlet, "DEFAULT")
        $("#OutletLoadingSalesCN_" + kodeOutlet).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i])
            }
        }
        var pareto1 = '#OutletSalesCN_' + kodeOutlet + '_Pareto1',
            pareto2 = '#OutletSalesCN_' + kodeOutlet + '_Pareto2',
            pareto3 = '#OutletSalesCN_' + kodeOutlet + '_Pareto3'
        getPareto('CUSTOMER', data.pareto1, pareto1)
        getPareto('PRODUK', data.pareto2, pareto2)
        getPareto('CALL', data.pareto3, pareto3)

        var chart = am4core.create('OutletChartSalesCN_' + kodeOutlet, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();
        valueAxis.numberFormatter.numberFormat = '#a';
        valueAxis.numberFormatter.bigNumberPrefixes = [{
                "number": 1e+3,
                "suffix": " RB"
            },
            {
                "number": 1e+6,
                "suffix": " JT"
            },
            {
                "number": 1e+9,
                "suffix": " M"
            }
        ];

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#OutletChartSalesCN_' + kodeOutlet).removeClass("unloaded")
    }
}

// TODO Outlet - Customer
async function setOutletChartCustomer(kodeOutlet) {
    kodeOutlet = clearSpace(kodeOutlet)
    if ($('#OutletChartCustomer_' + kodeOutlet).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getOutletChart(kodeOutlet, "CUSTOMER")
        $("#OutletLoadingCustomer_" + kodeOutlet).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "OUTLET",
                "kode": kodeOutlet,
                "jenis1": "CUSTOMER",
                "jenis2": "PRODUK",
                "jenis3": "CALL",
                "pareto1": "#OutletCustomer_" + kodeOutlet + "_Pareto1",
                "pareto2": "#OutletCustomer_" + kodeOutlet + "_Pareto2",
                "pareto3": "#OutletCustomer_" + kodeOutlet + "_Pareto3",
            }
        }

        var chart = am4core.create('OutletChartCustomer_' + kodeOutlet, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#OutletChartCustomer_' + kodeOutlet).removeClass("unloaded")
    }
}

// TODO Outlet - Product
async function setOutletChartProduct(kodeOutlet) {
    kodeOutlet = clearSpace(kodeOutlet)
    if ($('#OutletChartProduct_' + kodeOutlet).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getOutletChart(kodeOutlet, "PRODUK")
        $("#OutletLoadingProduct_" + kodeOutlet).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "OUTLET",
                "kode": kodeOutlet,
                "jenis1": "PRODUK",
                "jenis2": "CUSTOMER",
                "jenis3": "CALL",
                "pareto1": "#OutletProduct_" + kodeOutlet + "_Pareto1",
                "pareto2": "#OutletProduct_" + kodeOutlet + "_Pareto2",
                "pareto3": "#OutletProduct_" + kodeOutlet + "_Pareto3",
            }
        }

        var chart = am4core.create('OutletChartProduct_' + kodeOutlet, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#OutletChartProduct_' + kodeOutlet).removeClass("unloaded")
    }
}

// TODO Outlet - Call
async function setOutletChartFF(kodeOutlet) {
    kodeOutlet = clearSpace(kodeOutlet)
    if ($('#OutletChartFF_' + kodeOutlet).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getOutletChart(kodeOutlet, "CALL")
        $("#OutletLoadingFF_" + kodeOutlet).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "OUTLET",
                "kode": kodeOutlet,
                "jenis1": "FF",
                "jenis2": "TANGGAL",
                "jenis3": "CALL",
                "pareto1": "#OutletFF_" + kodeOutlet + "_Pareto1",
                "pareto2": "#OutletFF_" + kodeOutlet + "_Pareto2",
                "pareto3": "#OutletFF_" + kodeOutlet + "_Pareto3",
            }
        }

        var chart = am4core.create('OutletChartFF_' + kodeOutlet, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#OutletChartFF_' + kodeOutlet).removeClass("unloaded")
    }
}

// REVIEW Outlet
async function setOutletChart(search, filter) {
    showLoading()
    await $('#outletMenu').dashboardSetDataFromAPI(DashboardAPI.getOutlet, search, filter, item => {
        var Nilai = nominal(item.Nilai)
        var textSales = textStatistic(item.FlagCN),
            textProduct = textStatistic(item.FlagProd),
            textCustomer = textStatistic(item.FlagCust),
            textCall = textStatistic(item.FlagCall)
        var imageSales = imageStatistic(item.FlagCN),
            imageProduct = imageStatistic(item.FlagProd),
            imageCustomer = imageStatistic(item.FlagCust),
            imageCall = imageStatistic(item.FlagCall)
        html = `<div class="Filter OutletFilterName" data-filter="${item.Keterangan}" data-filterName="${item.NamaOutlet}"
            data-1="${item.Nilai}" data-2="${item.JmlProduk}" data-3="${item.JmlCust}" data-4="${item.JmlCall}">
            <button class="list-group-item list-group-item-action mt-2 border-dark" id="${item.KodeOutlet}">
                <div class="row rounded">
                    <div class="col-sm-4 media" data-toggle="collapse" data-target="#OutletSalesCN_${item.KodeOutlet}"
                        onclick="setOutletChartSalesCN('${item.KodeOutlet}')"><img src="src/assets/image/outlet-placeholder.jpg"
                            class="img-fluid rounded-circle mr-2" style="width: 40px;">
                        <div class="">
                            <strong>${item.NamaOutlet}</strong>
                            <p class="font-weight-normal text-info">${item.NamaSektor}</p>
                        </div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#OutletSalesCN_${item.KodeOutlet}"
                        onclick="setOutletChartSalesCN('${item.KodeOutlet}')">
                        <h4 class="text-center ${textSales}"><img src="${imageSales}" class="img-fluid mr-2"
                                style="width: 14px;">${Nilai}
                        </h4>
                        <div class="small text-center text-info">SALES</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#OutletCustomer_${item.KodeOutlet}"
                        onclick="setOutletChartCustomer('${item.KodeOutlet}')">
                        <h4 class="text-center ${textCustomer}"><img src="${imageCustomer}" class="img-fluid mr-2"
                                style="width: 14px;">${item.JmlCust}
                        </h4>
                        <div class="small text-center text-info">CUSTOMER</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#OutletProduct_${item.KodeOutlet}"
                        onclick="setOutletChartProduct('${item.KodeOutlet}')">
                        <h4 class="text-center ${textProduct}"><img src="${imageProduct}" class="img-fluid mr-2"
                                style="width: 14px;">${item.JmlProduk}
                        </h4>
                        <div class="small text-center text-info">PRODUK</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#OutletFF_${item.KodeOutlet}"
                        onclick="setOutletChartFF('${item.KodeOutlet}')">
                        <h4 class="text-center ${textCall}"><img src="${imageCall}" class="img-fluid mr-2"
                                style="width: 14px;">${item.JmlCall}
                        </h4>
                        <div class="small text-center text-info">CALL</div>
                    </div>
                </div>
            </button>
            <div id="mainChart_${item.KodeOutlet}" id="${item.KodeOutlet}" data-filter="${item.Keterangan}"
                data-filterName="${item.NamaOutlet}">
                <div id="OutletSalesCN_${item.KodeOutlet}" data-parent="#mainChart_${item.KodeOutlet}"
                    class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>SALES</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="OutletLoadingSalesCN_${item.KodeOutlet}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="OutletChartSalesCN_${item.KodeOutlet}" class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="OutletSalesCN_${item.KodeOutlet}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="OutletSalesCN_${item.KodeOutlet}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="OutletSalesCN_${item.KodeOutlet}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="OutletProduct_${item.KodeOutlet}" data-parent="#mainChart_${item.KodeOutlet}"
                    class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>PRODUK</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="OutletLoadingProduct_${item.KodeOutlet}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="OutletChartProduct_${item.KodeOutlet}" class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="OutletProduct_${item.KodeOutlet}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="OutletProduct_${item.KodeOutlet}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-12 border-top border-info">
                            <div class="list-group pt-1 mt-2" id="OutletProduct_${item.KodeOutlet}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="OutletCustomer_${item.KodeOutlet}" data-parent="#mainChart_${item.KodeOutlet}"
                    class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>CUSTOMER</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="OutletLoadingCustomer_${item.KodeOutlet}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="OutletChartCustomer_${item.KodeOutlet}" class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="OutletCustomer_${item.KodeOutlet}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="OutletCustomer_${item.KodeOutlet}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-12 border-top border-info">
                            <div class="list-group pt-1 mt-2" id="OutletCustomer_${item.KodeOutlet}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="OutletFF_${item.KodeOutlet}" data-parent="#mainChart_${item.KodeOutlet}"
                    class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>CALL</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="OutletLoadingFF_${item.KodeOutlet}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="OutletChartFF_${item.KodeOutlet}" class="unloaded"></div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="OutletFF_${item.KodeOutlet}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="OutletFF_${item.KodeOutlet}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="OutletFF_${item.KodeOutlet}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`
        return html;
    })
    $('#outletHeader').removeClass("unloaded")
    endLoading()
    windowWidth()
    if (search != undefined) {
        $('#outletFilter').html('<b>Filter by name :</b> "' + search + '" (Sales > 0)')
    } else if (filter != undefined) {
        $('#outletFilter').html('<b>Filter by category :</b> "' + filter + '" (Sales > 0)')
    } else {
        $('#outletFilter').html('<b>Filter by :</b> Top 10 Sales')
    }
}

// TODO Product - Sales CN
async function setProductChartSalesCN(kodeProduct) {
    kodeProduct = clearSpace(kodeProduct)
    if ($('#ProductChartSalesCN_' + kodeProduct).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getProductChart(kodeProduct, "DEFAULT")
        $("#ProductLoadingSalesCN_" + kodeProduct).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i])
            }
        }
        var pareto1 = '#ProductSalesCN_' + kodeProduct + '_Pareto1',
            pareto2 = '#ProductSalesCN_' + kodeProduct + '_Pareto2',
            pareto3 = '#ProductSalesCN_' + kodeProduct + '_Pareto3'
        getPareto('CUSTOMER', data.pareto1, pareto1)
        getPareto('OUTLET', data.pareto2, pareto2)
        getPareto('FF', data.pareto3, pareto3)

        var chart = am4core.create('ProductChartSalesCN_' + kodeProduct, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();
        valueAxis.numberFormatter.numberFormat = '#a';
        valueAxis.numberFormatter.bigNumberPrefixes = [{
                "number": 1e+3,
                "suffix": " RB"
            },
            {
                "number": 1e+6,
                "suffix": " JT"
            },
            {
                "number": 1e+9,
                "suffix": " M"
            }
        ];

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#ProductChartSalesCN_' + kodeProduct).removeClass("unloaded")
    }
}

// TODO Product - Customer
async function setProductChartCustomer(kodeProduct) {
    kodeProduct = clearSpace(kodeProduct)
    if ($('#ProductChartCustomer_' + kodeProduct).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getProductChart(kodeProduct, "CUSTOMER")
        $("#ProductLoadingCustomer_" + kodeProduct).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "PRODUK",
                "kode": kodeProduct,
                "jenis1": "CUSTOMER",
                "jenis2": "OUTLET",
                "jenis3": "FF",
                "pareto1": "#ProductCustomer_" + kodeProduct + "_Pareto1",
                "pareto2": "#ProductCustomer_" + kodeProduct + "_Pareto2",
                "pareto3": "#ProductCustomer_" + kodeProduct + "_Pareto3",
            }
        }

        var chart = am4core.create('ProductChartCustomer_' + kodeProduct, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#ProductChartCustomer_' + kodeProduct).removeClass("unloaded")
    }
}

// TODO Product - Outlet
async function setProductChartOutlet(kodeProduct) {
    kodeProduct = clearSpace(kodeProduct)
    if ($('#ProductChartOutlet_' + kodeProduct).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getProductChart(kodeProduct, "OUTLET")
        $("#ProductLoadingOutlet_" + kodeProduct).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "PRODUK",
                "kode": kodeProduct,
                "jenis1": "OUTLET",
                "jenis2": "CUSTOMER",
                "jenis3": "FF",
                "pareto1": "#ProductOutlet_" + kodeProduct + "_Pareto1",
                "pareto2": "#ProductOutlet_" + kodeProduct + "_Pareto2",
                "pareto3": "#ProductOutlet_" + kodeProduct + "_Pareto3",
            }
        }

        var chart = am4core.create('ProductChartOutlet_' + kodeProduct, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#ProductChartOutlet_' + kodeProduct).removeClass("unloaded")
    }
}

// TODO Product - FF
async function setProductChartFF(kodeProduct) {
    kodeProduct = clearSpace(kodeProduct)
    if ($('#ProductChartFF_' + kodeProduct).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getProductChart(kodeProduct, "FF")
        $("#ProductLoadingFF_" + kodeProduct).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "PRODUK",
                "kode": kodeProduct,
                "jenis1": "FF",
                "jenis2": "CUSTOMER",
                "jenis3": "OUTLET",
                "pareto1": "#ProductFF_" + kodeProduct + "_Pareto1",
                "pareto2": "#ProductFF_" + kodeProduct + "_Pareto2",
                "pareto3": "#ProductFF_" + kodeProduct + "_Pareto3",
            }
        }

        var chart = am4core.create('ProductChartFF_' + kodeProduct, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#ProductChartFF_' + kodeProduct).removeClass("unloaded")
    }
}

// REVIEW Product
async function setProductChart(search, filter) {
    showLoading()
    await $('#productMenu').dashboardSetDataFromAPI(DashboardAPI.getProduct, search, filter, item => {
        var Nilai = nominal(item.Nilai)
        var textSales = textStatistic(item.FlagCN),
            textCustomer = textStatistic(item.FlagCust),
            textOutlet = textStatistic(item.FlagOutlet),
            textCall = textStatistic(item.FlagCall)
        var imageSales = imageStatistic(item.FlagCN),
            imageCustomer = imageStatistic(item.FlagCust),
            imageOutlet = imageStatistic(item.FlagOutlet),
            imageCall = imageStatistic(item.FlagCall)
        html = `<div class="Filter ProductFilterName" data-filter="${item.Keterangan}" data-filterName="${item.NamaProduct}"
            data-1="${item.Nilai}" data-2="${item.JmlCust}" data-3="${item.JmlOutlet}" data-4="${item.JmlFF}">
            <button class="list-group-item list-group-item-action mt-2 border-dark" id="${item.KodeProduk}">
                <div class="row rounded">
                    <div class="col-sm-4 media" data-toggle="collapse" data-target="#ProductSalesCN_${item.KodeProduk}"
                        onclick="setProductChartSalesCN('${item.KodeProduk}')"><img
                            src="src/assets/image/produk-placeholder.jpg" class="img-fluid rounded-circle mr-2"
                            style="width: 40px;">
                        <div class="">
                            <strong>${item.NamaProduk}</strong>
                            <p class="font-weight-normal text-info"></p>
                        </div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#ProductSalesCN_${item.KodeProduk}"
                        onclick="setProductChartSalesCN('${item.KodeProduk}')">
                        <h4 class="text-center ${textSales}"><img src="${imageSales}" class="img-fluid mr-2"
                                style="width: 14px;">${Nilai}
                        </h4>
                        <div class="small text-center text-info">SALES</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#ProductCustomer_${item.KodeProduk}"
                        onclick="setProductChartCustomer('${item.KodeProduk}')">
                        <h4 class="text-center ${textCustomer}"><img src="${imageCustomer}" class="img-fluid mr-2"
                                style="width: 14px;">${item.JmlCust}
                        </h4>
                        <div class="small text-center text-info">CUSTOMER</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#ProductOutlet_${item.KodeProduk}"
                        onclick="setProductChartOutlet('${item.KodeProduk}')">
                        <h4 class="text-center ${textOutlet}"><img src="${imageOutlet}" class="img-fluid mr-2"
                                style="width: 14px;">${item.JmlOutlet}
                        </h4>
                        <div class="small text-center text-info">OUTLET</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#ProductFF_${item.KodeProduk}"
                        onclick="setProductChartFF('${item.KodeProduk}')">
                        <h4 class="text-center ${textCall}"><img src="${imageCall}" class="img-fluid mr-2"
                                style="width: 14px;">${item.JmlFF}
                        </h4>
                        <div class="small text-center text-info">FF</div>
                    </div>
                </div>
            </button>
            <div id="mainChart_${item.KodeProduk}" id="${item.KodeProduk}"
                data-filterName="${item.NamaProduk}">
                <div id="ProductSalesCN_${item.KodeProduk}" data-parent="#mainChart_${item.KodeProduk}"
                    class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>SALES</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="ProductLoadingSalesCN_${item.KodeProduk}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="ProductChartSalesCN_${item.KodeProduk}"
                                class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="ProductSalesCN_${item.KodeProduk}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="ProductSalesCN_${item.KodeProduk}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="ProductSalesCN_${item.KodeProduk}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ProductCustomer_${item.KodeProduk}" data-parent="#mainChart_${item.KodeProduk}"
                    class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>CUSTOMER</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="ProductLoadingCustomer_${item.KodeProduk}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="ProductChartCustomer_${item.KodeProduk}"
                                class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="ProductCustomer_${item.KodeProduk}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="ProductCustomer_${item.KodeProduk}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-12 border-top border-info">
                            <div class="list-group pt-1 mt-2" id="ProductCustomer_${item.KodeProduk}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ProductOutlet_${item.KodeProduk}" data-parent="#mainChart_${item.KodeProduk}"
                    class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>OUTLET</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="ProductLoadingOutlet_${item.KodeProduk}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="ProductChartOutlet_${item.KodeProduk}" class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="ProductOutlet_${item.KodeProduk}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="ProductOutlet_${item.KodeProduk}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-12 border-top border-info">
                            <div class="list-group pt-1 mt-2" id="ProductOutlet_${item.KodeProduk}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ProductFF_${item.KodeProduk}" data-parent="#mainChart_${item.KodeProduk}"
                    class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>FF</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="ProductLoadingFF_${item.KodeProduk}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="ProductChartFF_${item.KodeProduk}" class="unloaded">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row mx-2">
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="ProductFF_${item.KodeProduk}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="ProductFF_${item.KodeProduk}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="ProductFF_${item.KodeProduk}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`
        return html;
    })
    $('#productHeader').removeClass("unloaded")
    endLoading()
    windowWidth()
    if (search != undefined) {
        $('#productFilter').html('<b>Filter by name :</b> "' + search + '" (Sales > 0)')
    } else if (filter != undefined) {
        $('#productFilter').html('<b>Filter by category :</b> "' + filter + '" (Sales > 0)')
    } else {
        $('#productFilter').html('<b>Filter by :</b> Top 10 Sales')
    }
}

// TODO FF - Sales CN
async function setFFChartSalesCN(kodeFF) {
    kodeFF = clearSpace(kodeFF)
    if ($('#FFChartSalesCN_' + kodeFF).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getFFChart(kodeFF, "DEFAULT")
        $("#FFLoadingSalesCN_" + kodeFF).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i])
            }
        }
        var pareto1 = '#FFSalesCN_' + kodeFF + '_Pareto1',
            pareto2 = '#FFSalesCN_' + kodeFF + '_Pareto2',
            pareto3 = '#FFSalesCN_' + kodeFF + '_Pareto3'
        getPareto('CUSTOMER', data.pareto1, pareto1)
        getPareto('OUTLET', data.pareto2, pareto2)
        getPareto('PRODUK', data.pareto3, pareto3)

        var chart = am4core.create('FFChartSalesCN_' + kodeFF, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();
        valueAxis.numberFormatter.numberFormat = '#a';
        valueAxis.numberFormatter.bigNumberPrefixes = [{
                "number": 1e+3,
                "suffix": " RB"
            },
            {
                "number": 1e+6,
                "suffix": " JT"
            },
            {
                "number": 1e+9,
                "suffix": " M"
            }
        ];

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#FFChartSalesCN_' + kodeFF).removeClass("unloaded")
    }
}

// TODO FF - Customer
async function setFFChartCustomer(kodeFF) {
    kodeFF = clearSpace(kodeFF)
    if ($('#FFChartCustomer_' + kodeFF).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getFFChart(kodeFF, "CUSTOMER")
        $("#FFLoadingCustomer_" + kodeFF).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "FF",
                "kode": kodeFF,
                "jenis1": "CUSTOMER",
                "jenis2": "PRODUK",
                "jenis3": "OUTLET",
                "pareto1": "#FFCustomer_" + kodeFF + "_Pareto1",
                "pareto2": "#FFCustomer_" + kodeFF + "_Pareto2",
                "pareto3": "#FFCustomer_" + kodeFF + "_Pareto3",
            }
        }

        var chart = am4core.create('FFChartCustomer_' + kodeFF, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#FFChartCustomer_' + kodeFF).removeClass("unloaded")
    }
}

// TODO FF - Outlet
async function setFFChartOutlet(kodeFF) {
    kodeFF = clearSpace(kodeFF)
    if ($('#FFChartOutlet_' + kodeFF).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getFFChart(kodeFF, "OUTLET")
        $("#FFLoadingOutlet_" + kodeFF).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "FF",
                "kode": kodeFF,
                "jenis1": "OUTLET",
                "jenis2": "PRODUK",
                "jenis3": "CUSTOMER",
                "pareto1": "#FFOutlet_" + kodeFF + "_Pareto1",
                "pareto2": "#FFOutlet_" + kodeFF + "_Pareto2",
                "pareto3": "#FFOutlet_" + kodeFF + "_Pareto3",
            }
        }

        var chart = am4core.create('FFChartOutlet_' + kodeFF, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#FFChartOutlet_' + kodeFF).removeClass("unloaded")
    }
}

// TODO FF - Product
async function setFFChartProduct(kodeFF) {
    kodeFF = clearSpace(kodeFF)
    if ($('#FFChartProduct_' + kodeFF).hasClass("unloaded") == true) {
        var data = await DashboardAPI.getFFChart(kodeFF, "PRODUK")
        $("#FFLoadingProduct_" + kodeFF).remove();
        var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
            praData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            praYTD = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            chartData = []
        convertMonth(data, praData, praYTD)
        for (i = 0; i < 12; i++) {
            chartData[i] = {
                "valuePeriode": new Date().getFullYear().toString() + months[i],
                "ytdPeriode": ((new Date().getFullYear()) - 1).toString() + months[i],
                "month": getMonth(months[i]),
                "value": praData[i],
                "valueNominal": nominal(praData[i]),
                "ytd": praYTD[i],
                "ytdNominal": nominal(praYTD[i]),
                "menu": "FF",
                "kode": kodeFF,
                "jenis1": "PRODUK",
                "jenis2": "CUSTOMER",
                "jenis3": "OUTLET",
                "pareto1": "#FFProduct_" + kodeFF + "_Pareto1",
                "pareto2": "#FFProduct_" + kodeFF + "_Pareto2",
                "pareto3": "#FFProduct_" + kodeFF + "_Pareto3",
            }
        }

        var chart = am4core.create('FFChartProduct_' + kodeFF, am4charts.XYChart);

        chart.data = chartData
        chart.logo.height = -15

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.startLocation = 0.3;
        categoryAxis.endLocation = 0.7;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.numberFormatter = new am4core.NumberFormatter();

        var fillModifier = new am4core.LinearGradientModifier();
        fillModifier.opacities = [1, 0];
        fillModifier.offsets = [0, 1];
        fillModifier.gradient.rotation = 90;

        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = "value";
        series1.dataFields.categoryX = "month";
        series1.name = new Date().getFullYear().toString();
        series1.strokeWidth = 3;
        series1.tensionX = 0.7;
        series1.bullets.push(new am4charts.CircleBullet());
        series1.tooltipText = new Date().getFullYear().toString() + " : [bold]{valueNominal}[/]";
        // series1.fillOpacity = 1;
        // series1.segments.template.fillModifier = fillModifier;
        series1.tooltip.label.interactionsEnabled = true;
        series1.tooltip.keepTargetHover = true;
        series1.tooltip.pointerOrientation = "vertical";
        series1.tooltip.label.textAlign = "middle";
        series1.tooltip.label.textValign = "middle";
        series1.tooltip.events.on("hit", getParetoN, this);

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "ytd";
        series2.dataFields.categoryX = "month";
        series2.name = ((new Date().getFullYear()) - 1).toString();
        series2.strokeWidth = 1;
        series2.tensionX = 0.7;
        series2.bullets.push(new am4charts.CircleBullet());
        series2.tooltipText = ((new Date().getFullYear()) - 1).toString() + " : [bold]{ytdNominal}[/]";
        series2.stroke = am4core.color("rgba(255, 50, 50, 1)")
        series2.fill = am4core.color("rgba(255, 50, 50, 1)")
        series2.strokeDasharray = "3,4";
        series2.tooltip.label.interactionsEnabled = true;
        series2.tooltip.keepTargetHover = true;
        series2.tooltip.pointerOrientation = "vertical";
        series2.tooltip.label.textAlign = "middle";
        series2.tooltip.label.textValign = "middle";
        series2.tooltip.events.on("hit", getParetoN_1, this);

        chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "none";
        chart.legend = new am4charts.Legend();
        $('#FFChartProduct_' + kodeFF).removeClass("unloaded")
    }
}

// REVIEW FF
async function setFFChart(search, filter) {
    showLoading()
    await $('#FFMenu').dashboardSetDataFromAPI(DashboardAPI.getFF, search, filter, item => {
        var Nilai = nominal(item.Nilai)
        var textSales = textStatistic(item.FlagCN),
            textCustomer = textStatistic(item.FlagCust),
            textOutlet = textStatistic(item.FlagOutlet),
            textProduct = textStatistic(item.FlagProd)
        var imageSales = imageStatistic(item.FlagCN),
            imageCustomer = imageStatistic(item.FlagCust),
            imageOutlet = imageStatistic(item.FlagOutlet),
            imageProduct = imageStatistic(item.FlagProd)
        html = `<div class="Filter FFFilterName" data-filter="${item.Keterangan}" data-filterName="${item.Nama}" data-1="${item.Nilai}"
            data-2="${item.JmlCust}" data-3="${item.JmlOutlet}" data-4="${item.JmlProduk}">
            <button class="list-group-item list-group-item-action mt-2 border-dark" id="${item.KodeArea}">
                <div class="row rounded">
                    <div class="col-sm-4 media" data-toggle="collapse" data-target="#FFSalesCN_${item.KodeArea}"
                        onclick="setFFChartSalesCN('${item.KodeArea}'" )><img src="src/assets/image/employee-placeholder.jpg"
                            class="img-fluid rounded-circle mr-2" style="width: 40px;">
                        <div class="">
                            <strong>${item.Nama}</strong>
                            <p class="font-weight-normal text-info">${item.NamaAM}</p>
                        </div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#FFSalesCN_${item.KodeArea}"
                        onclick="setFFChartSalesCN('${item.KodeArea}')">
                        <h4 class="text-center ${textSales}"><img src="${imageSales}" class="img-fluid mr-2"
                                style="width: 14px;">${Nilai}
                        </h4>
                        <div class="small text-center text-info">SALES</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#FFCustomer_${item.KodeArea}"
                        onclick="setFFChartCustomer('${item.KodeArea}')">
                        <h4 class="text-center ${textCustomer}"><img src="${imageCustomer}" class="img-fluid mr-2"
                                style="width: 14px;">${item.JmlCust}
                        </h4>
                        <div class="small text-center text-info">CUSTOMER</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#FFOutlet_${item.KodeArea}"
                        onclick="setFFChartOutlet('${item.KodeArea}')">
                        <h4 class="text-center ${textOutlet}"><img src="${imageOutlet}" class="img-fluid mr-2"
                                style="width: 14px;">${item.JmlOutlet}
                        </h4>
                        <div class="small text-center text-info">OUTLET</div>
                    </div>
                    <div class="col-sm-2 border-left main-button" data-toggle="collapse" data-target="#FFProduct_${item.KodeArea}"
                        onclick="setFFChartProduct('${item.KodeArea}')">
                        <h4 class="text-center ${textProduct}"><img src="${imageProduct}" class="img-fluid mr-2"
                                style="width: 14px;">${item.JmlProduk}
                        </h4>
                        <div class="small text-center text-info">PRODUK</div>
                    </div>
                </div>
            </button>
            <div id="mainChart_${item.KodeArea}" id="${item.KodeArea}"
                data-filter="${item.Keterangan}" data-filterName="${item.Nama}">
                <div id="FFSalesCN_${item.KodeArea}" data-parent="#mainChart_${item.KodeArea}" class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>SALES</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="FFLoadingSalesCN_${item.KodeArea}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="FFChartSalesCN_${item.KodeArea}" class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="FFSalesCN_${item.KodeArea}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="FFSalesCN_${item.KodeArea}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="FFSalesCN_${item.KodeArea}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="FFCustomer_${item.KodeArea}" data-parent="#mainChart_${item.KodeArea}" class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>CUSTOMER</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="FFLoadingCustomer_${item.KodeArea}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="FFChartCustomer_${item.KodeArea}" class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="FFCustomer_${item.KodeArea}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="FFCustomer_${item.KodeArea}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-12 border-top border-info">
                            <div class="list-group pt-1 mt-2" id="FFCustomer_${item.KodeArea}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="FFOutlet_${item.KodeArea}" data-parent="#mainChart_${item.KodeArea}" class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>OUTLET</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="FFLoadingOutlet_${item.KodeArea}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="FFChartOutlet_${item.KodeArea}" class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="FFOutlet_${item.KodeArea}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-group pt-1 my-2" id="FFOutlet_${item.KodeArea}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-12 border-top border-info">
                            <div class="list-group pt-1 mt-2" id="FFOutlet_${item.KodeArea}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="FFProduct_${item.KodeArea}" data-parent="#mainChart_${item.KodeArea}" class="collapse bg-white py-2">
                    <div class="row mx-2">
                        <div class="col-12 text-center text-info">
                            <h5><strong>PRODUK</strong></h5>
                        </div>
                        <div class="col-12 text-center" id="FFLoadingProduct_${item.KodeArea}">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="height: 400px;width: 100%;" id="FFChartProduct_${item.KodeArea}" class="unloaded">
                            </div>
                        </div>
                    </div>
                    <div class="row mx-2">
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="FFProduct_${item.KodeArea}_Pareto1">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="FFProduct_${item.KodeArea}_Pareto2">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="list-group pt-1" id="FFProduct_${item.KodeArea}_Pareto3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`
        return html;
    })
    $('#FFHeader').removeClass("unloaded")
    endLoading()
    windowWidth()
    if (search != undefined) {
        $('#ffFilter').html('<b>Filter by name :</b> "' + search + '" (Sales > 0)')
    } else if (filter != undefined) {
        $('#ffFilter').html('<b>Filter by category :</b> "' + filter + '" (Sales > 0)')
    } else {
        $('#ffFilter').html('<b>Filter by :</b> Top 10 Sales')
    }
}

// ANCHOR Running
setCustomerPieChart()
setCustomerChart();

$("#outletButton").click(function () {
    if ($('#outletHeader').hasClass("unloaded") == true) {
        setOutletPieChart()
        setOutletChart();
    }
});

$("#productButton").click(function () {
    if ($('#productHeader').hasClass("unloaded") == true) {
        setProductPieChart()
        setProductChart();
    }
});

$("#ffButton").click(function () {
    if ($('#FFHeader').hasClass("unloaded") == true) {
        setFFPieChart()
        setFFChart();
    }
});