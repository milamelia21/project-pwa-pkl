$('#page-title').titlePage({
    icon: 'dashboard',
    iconColor: ['#84fab0', '#8fd3f4'],
    title: 'Analytics Dashboard',
    subtitle: 'Analytics is the systemic computational analysis of data or statistics. It is used for the discovery, interpretation, and communication of meaningful patterns in data.'
});

$('#page-content').load('src/views/pages/menu/administration/dashboard/analytics/analytics-charts.html')