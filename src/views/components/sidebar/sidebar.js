var isSidebarClosed = $(window).width() < 1200;

function closedSidebarUpdate() {
    if (isSidebarClosed) {
        $('#toggle-closed-sidebar').addClass('is-active');
        $('.app-container').addClass('closed-sidebar');
    } else {
        $('#toggle-closed-sidebar').removeClass('is-active');
        $('.app-container').removeClass('closed-sidebar')
    }
}

closedSidebarUpdate();

// window listener
$(window).on('resize', function () {
    var win = $(this);
    var isSidebarClosedNew = win.width() < 1200;
    if (isSidebarClosedNew != isSidebarClosed) {
        isSidebarClosed = isSidebarClosedNew
        closedSidebarUpdate();
    }
});



// sidebar hover
$('.app-sidebar').hover(function () {
    $('.app-container').addClass('closed-sidebar-open')
}, function () {
    $('.app-container').removeClass('closed-sidebar-open');
    if ($('.app-container').hasClass('closed-sidebar'))
        setTimeout(function () {
            $('.collapse').collapse('hide');
        }, 200);
});

// sidebar closed control
$('#toggle-closed-sidebar').click(function (e) {
    e.preventDefault();
    $('#toggle-closed-sidebar').toggleClass('is-active');
    $('.app-container').toggleClass('closed-sidebar')

});