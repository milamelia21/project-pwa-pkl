/**
 * @example
 * requestWithToken({
        showMessageFailure: true,
        showSuccessAlert: true,
        showLoading: true,
        method: 'DELETE',
        api: 'menu',
        body: {
            sNamaSubMenu: subMenu,
            sGroupMenu: groupMenu,
            sNamaMenu: menu,
            sPathUrl: path,
            sIcon: icon,
        }
    });
 */

async function requestWithToken(options) {
    var statusCode;
    if (options.showLoading)
        Swal.fire({
            title: 'Please Wait !',
            text: 'processing your request',
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading()
            },
        });
    var formdata = typeof options.body !== 'undefined' && ['POST', 'PUT', 'DELETE'].includes(options.method) ? new FormData() : null;
    var param = '';
    if (['POST', 'PUT', 'DELETE'].includes(options.method)) {
        for (var key in options.body) {
            var value = options.body[key];
            if (Array.isArray(value)) {
                for (var index = 0; index < value.length; index++) {
                    formdata.append(key, value[index]);
                }
            } else {
                formdata.append(key, value);
            }
        }
    }
    if (options.method == 'GET') {
        param = new URLSearchParams(options.body).toString();
        if (param != '')
            param = '?' + param
    }
    var requestOptions = {
        method: options.method,
        headers: {
            'Authorization': 'Bearer ' + Auth.access_token()
        },
        body: typeof options.body == "string" ? options.body : formdata,
    };
    try {
        var response = await fetch(hostAPI + options.api + param, requestOptions);
        statusCode = response.status;
        var data = await response.json();

        if (options.showLoading)
            swal.close();
        if (statusCode == 200) {
            if (options.showSuccessAlert) {
                    if (typeof data.data == "string") {
                        if (data.data != "OK") {
                            MessageBox.flashError(data.note ? data.note : null);
                        } else {
                            MessageBox.flashSuccess();
                        }
                    } else {
                        MessageBox.flashSuccess();
                    }
            } else {
                if (typeof data.data == "string") {
                    if (data.data != "OK")
                        MessageBox.flashError(data.note ? data.note : null);
                }
                if (data.status == "0") {
                    MessageBox.flashError(data.note ? data.note : null);
                }
            }
            return data;
        } else if (statusCode == 401) {
            // debugger
            sessionStorage.removeItem('access_token');
            sessionStorage.removeItem('userInfo');
            Navigator.push('/');
            MessageBox.tokenExpired(data.note ? data.note : null)
        } else if (statusCode == 400 || 500) {
            MessageBox.flashError(data.message ? data.message : null)
        } else {
            MessageBox.flashError(data.note ? data.note : null);
        }
    } catch (error) {
        if (statusCode == 400) {
            Auth.loggedOut()
            MessageBox.flashError();
        }
        return null
    }
}

async function requestPhotoWithToken(nipKaryawan, dataImage, dataPhoto) {
    var photoToken;
    var loginformdata = new FormData();
    loginformdata.append('username', '2180840');
    loginformdata.append('password', '123456');
    loginformdata.append('company', 'vneu');

    var loginrequestOptions = {
        method: 'GET',
        headers: {
            'Authorization': 'Basic MTUwMTAwMDE6YW1pa3VpYnU='
        },
        body: loginformdata,
    }

    try {
        var response = await fetch('http://103.106.145.14:8080/login', loginrequestOptions);
        statusCode = response.status;
        var data = await response.json();
        photoToken = data.access_token
    } catch (error) {
        console.log(error)
    }

    var formdata = new FormData();
    formdata.append('username', nipKaryawan);
    formdata.append('photo', dataPhoto, 'image.png');

    var requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer ' + photoToken
        },
        body: formdata,
    }

    try {
        var response = await fetch('http://103.106.145.14:8080/registerphoto', requestOptions);
        statusCode = response.status;
        var data = await response.json();
        console.log(data);
        await TakePhoto.get(
            dataImage,
            data.message.face_detected,
            data.message.face_qty
        )
        return data;
    } catch (error) {
        console.log(error)
        MessageBox.flashError();
    }
}