function UploadZone(option) {
    return new Dropzone(".dropzone", {
        url: '/file/post',
        addRemoveLinks: true,
        autoProcessQueue: false,
        acceptedFiles: option.acceptedFiles.join(','),
        maxFiles: 1,
        maxfilesexceeded: function (file) {
            this.removeAllFiles();
            this.addFile(file);
        },
    })
}