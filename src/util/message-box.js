class MessageBox {
    static flashError(message) {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: message || 'Oops! Something went wrong',
            showConfirmButton: false,
            timer: 10000
        })
    }

    static flashSuccess() {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Successfully Submit',
            showConfirmButton: false,
            timer: 1500
        })
    }

    static tokenExpired(message) {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: message || 'Oops! Your token expired',
            showConfirmButton: false,
            timer: 10000
        })
    }

    static async confirm(text) {
        var result = await Swal.fire({
            title: 'Are you sure?',
            text: text || "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        })
        return result.value;
    }

    static async ubahstatus(text) {
        var result = await Swal.fire({
            title: 'Are you sure?',
            text: text || " Will Change This Material's Status!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        })
        return result.value;
    }

    static async ubahstatusSpesifikasi(text) {
        var result = await Swal.fire({
            title: 'Are you sure?',
            text: text || "Change the Specification Status to OBSOLETE !",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        })
        return result.value;
    }

    static async textArea(text) {
        var result = await Swal.fire({
            title: text,
            input: 'textarea',
            inputPlaceholder: 'Type your message here...',
            inputAttributes: {
                'aria-label': 'Type your message here'
            },
            showCancelButton: true,
            cancelButtonColor: '#d33',
        })
        return result.value
    }

    static async textbox(text) {
        return await Swal.fire({
            title: text,
            input: 'text',
            inputPlaceholder: 'Type your message here...',
            inputAttributes: {
                'aria-label': 'Type your message here'
            },
            showCancelButton: true,
            cancelButtonColor: '#d33',
        })
    }
}