// Service Worker
// if ('serviceWorker' in navigator) {
//     window.addEventListener('load', function () {
//         navigator.serviceWorker.register('/sw.js').then(function (registration) {
//             console.log('ServiceWorker registration successful with scope: ', registration.scope);
//         }, function (err) {
//             console.log('ServiceWorker registration failed: ', err);
//         });
//     });
// }

// Route
const routes = [
    new Route('Login', '/', '/login', false),
    new Route('Dashboard', '/dashboard', '/dashboard', true),
    new Route('Print', '/print/:menuPrint/:data', '/print', true),
];


var INITIAL_DASHBOARD_PAGE = 'menu/main-navigation/dashboard/analytics/analytics';
router(routes);