class Area {
    static get(callback) {
        const url = `${hostAPI}area`
        var networkDataReceived = false;
        var networkUpdate = fetch(
            url, {
                headers: {
                    'Authorization': 'Bearer ' + Auth.access_token(),
                }
            }).then(function (response) {
            networkDataReceived = true;
            console.log('get data from network')
            callback(response.data);
        })

        caches.match(url).then(function (response) {
            if (!response)
                throw Error('no data on cache');
            else
                return response.json();
        }).then(function (data) {
            if (!networkDataReceived) {
                console.log('get data from cache')
                callback(data)
            }
        }).catch(function () {
            return networkUpdate;
        });
    }
}


class Auth {

    static userInfo() {
        return JSON.parse(sessionStorage.getItem('userInfo'))
    }

    static access_token() {
        return sessionStorage.getItem('access_token')
    }

    static menu() {
        return convertMenuFromJSON(JSON.parse(sessionStorage.getItem('menu')));
    }

    static isLoggedIn() {
        return sessionStorage.getItem('userInfo') != null
    }

    static loggedIn(nip, password) {
        // $.ajax({
        //     url: `${hostAPI}users/login`,
        //     type: 'GET',
        //     dataType: 'json',
        //     headers: {
        //         'Authorization': `Basic ${btoa(nip + ':' + password)}`
        //     },
            // success: function (response) {
                sessionStorage.setItem('menu', JSON.stringify());
                sessionStorage.setItem('access_token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdXVpZCI6ImJiZGY4ZTBiLTQ3ZGUtNDgxMC1hMjM5LWUxNTZkMGE0N2EzNCIsImF1dGhvcml6ZWQiOnRydWUsImV4cCI6MTY2MTEyOTY3NSwiaWQiOjEsIm5hbWUiOiJBZG1pbmlzdHJhdG9yIiwibmlwIjoiMTUwMTAwMDEiLCJyb2xlIjoiQURNSU5JU1RSQVRPUiJ9.6NKkqMfdAcnJOfhCmbeYvfqhWpVYv2CDO5WAMKT-CWc');
                sessionStorage.setItem('userInfo', JSON.stringify(parseJwt('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdXVpZCI6ImJiZGY4ZTBiLTQ3ZGUtNDgxMC1hMjM5LWUxNTZkMGE0N2EzNCIsImF1dGhvcml6ZWQiOnRydWUsImV4cCI6MTY2MTEyOTY3NSwiaWQiOjEsIm5hbWUiOiJBZG1pbmlzdHJhdG9yIiwibmlwIjoiMTUwMTAwMDEiLCJyb2xlIjoiQURNSU5JU1RSQVRPUiJ9.6NKkqMfdAcnJOfhCmbeYvfqhWpVYv2CDO5WAMKT-CWc')));
                Navigator.push('/dashboard');
  
        //     },
        // }).fail(function () {
        //     MessageBox.flashError();
        // });
    
    }

    static loggedOut() {
        Navigator.push('/');
        sessionStorage.removeItem('access_token');
        sessionStorage.removeItem('userInfo');
    }
}

class Permission {
    static async init() {
        if ($('#content').data('add') == "1") {
            $('.create-button').removeClass('invisible');
            $('.create-button').addClass('visible');
        }
        if ($('#content').data('edit') == "1") {
            $('.edit-button').removeClass('invisible');
            $('.edit-button').addClass('visible');
        }
        if ($('#content').data('delete') == "1") {
            $('.delete-button').removeClass('invisible');
            $('.delete-button').addClass('visible');
        }
        $('.create-button').click(function (e) {
            if ($('#content').data('add') != "1") {
                MessageBox.flashError("You dont have permission for this action")
                return;
            }
        })
        $('.edit-button').click(function (e) {
            if ($('#content').data('edit') != "1") {
                MessageBox.flashError("You dont have permission for this action")
                return;
            }
        })
        $('.delete-button').click(function (e) {
            if ($('#content').data('delete') != "1") {
                MessageBox.flashError("You dont have permission for this action")
                return;
            }
        })

    }
}

class master {
    static async getCities() {
        return await requestWithToken({
            method: 'GET',
            api: 'cities'
        });
    }

    static async createCities(id, namakota) {
        return await requestWithToken({
            showSuccessAlert: true,
            showLoading: true,
            method: 'POST',
            api: 'cities',
            body: JSON.stringify({
                id: id.toUpperCase(),
                name: namakota.toUpperCase()
            })
        })
    }

    static async updateCities(id, namakota) {
        return await requestWithToken({
            showSuccessAlert: true,
            showLoading: true,
            method: 'PUT',
            api: 'cities/' + id,
            body: JSON.stringify({
                name: namakota.toUpperCase()
            })
        })
    }

    static async deleteCisties(id) {
        return await requestWithToken({
            showSuccessAlert: true,
            showLoading: true,
            method: 'DELETE',
            api: 'cities/' + id,

        })
    }

}