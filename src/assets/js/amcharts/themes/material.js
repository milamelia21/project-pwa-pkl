am4internal_webpackJsonp(["8593"], {
    d66p: function (c, t, e) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var b = e("aCit"),
            o = e("8ZqG"),
            n = function (c) {
                Object(b.b)(c, "ColorSet") && (c.list = [Object(o.c)("#8BD9D9"), Object(o.c)("#3FBF48"), Object(o.c)("#F2E205"), Object(o.c)("#F23838"), Object(o.c)("#F2CA80"), Object(o.c)("#624CAB"), Object(o.c)("#38A3A5"), Object(o.c)("#57CC99"), Object(o.c)("#FFC857"), Object(o.c)("#DB5461"), Object(o.c)("#8BC34A"), Object(o.c)("#CDDC39"), Object(o.c)("#FFEB3B"), Object(o.c)("#FFC107"), Object(o.c)("#FF9800"), Object(o.c)("#FF5722"), Object(o.c)("#795548"), Object(o.c)("#9E9E9E"), Object(o.c)("#607D8B")], c.minLightness = .2, c.maxLightness = .7, c.reuse = !0), Object(b.b)(c, "ResizeButton") && (c.background.cornerRadiusTopLeft = 20, c.background.cornerRadiusTopRight = 20, c.background.cornerRadiusBottomLeft = 20, c.background.cornerRadiusBottomRight = 20), Object(b.b)(c, "Tooltip") && (c.animationDuration = 800)
            };
        window.am4themes_material = n
    }
}, ["d66p"]);