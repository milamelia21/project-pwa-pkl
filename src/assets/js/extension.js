jQuery.fn.extend({
    comboboxSearch: function () {
        $(this).each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        });
    },
    comboboxSearchCustom: function () {
        $(this).each(function () {
            $(this).select2({
                tags: true,
                theme: 'bootstrap4',
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        });
    },
    setOptionsFromAPI: async function (option) {
        var data = await option.api();
        const select = $(this);
        select.empty();
        select.append(`<option></option>`);
        if (data != undefined) {
            if (data.data.length == 0 && option.nodataValue != undefined) {
                select.append(`<option value="${option.nodataValue}" selected>${option.nodataValue}</option>`);
            } else {
                data.data.forEach(function (element) {
                    var dataOption = '';
                    if (option.data != undefined) {
                        var data = option.data(element)
                        for (var key in data) {
                            dataOption += `data-${key}="${data[key]}" `
                        }
                    }
                    if (element[option.value] == option.selected) {
                        select.append(`<option ${dataOption} value="${eval('element.'+option.value)}" selected>${eval('element.'+option.text)}</option>`);
                    } else {
                        select.append(`<option ${dataOption} value="${eval('element.'+option.value)}">${eval('element.'+option.text)}</option>`);
                    }
                })
            }
        } else if (option.nodataValue != undefined) {
            select.append(`<option value="${option.nodataValue}" selected>${option.nodataValue}</option>`);
        }
    },
    autocompleteOff: function () {
        $(this).attr('autocomplete', 'off')
    },
    datatable: function () {
        var id = $(this).attr('id')
        $('[aria-describedby="' + id + '_info"] .filters').remove();
        $('#' + id + ' thead tr')
            .clone(true)
            .addClass('filters')
            .appendTo('#' + id + ' thead');
        var datatable = $(this).DataTable({
            orderCellsTop: true,
            responsive: false,
            sFilterInput: 'form-control',
            scrollX: true,
            "bDestroy": true,
            initComplete: function () {
                var api = this.api();
                api
                    .columns()
                    .eq(0)
                    .each(function (colIdx) {
                        var cell = $('.filters th').eq(
                            $(api.column(colIdx).header()).index()
                        );
                        var title = $(cell).text();
                        if (title == 'Action' || title == '') {
                            $(cell).html('');
                            return
                        }
                        $(cell).html('<input type="text" class="form-control" placeholder="' + title + '" />');
                        $(
                                'input',
                                $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                            .off('keyup change')
                            .on('keyup change', function (e) {
                                e.stopPropagation();
                                $(this).attr('title', $(this).val());
                                var regexr = '({search})';
                                var cursorPosition = this.selectionStart;
                                api
                                    .column(colIdx)
                                    .search(
                                        this.value != '' ?
                                        regexr.replace('{search}', '(((' + this.value + ')))') :
                                        '',
                                        this.value != '',
                                        this.value == ''
                                    )
                                    .draw();
                                $(this)
                                    .focus()[0]
                                    .setSelectionRange(cursorPosition, cursorPosition);
                            });
                    });
            }
        });
        $('[data-toggle="tooltip"]').tooltip({
            placement: 'bottom',
            boundary: "window",
            container: "body"
        })
        $('[data-toggle="tooltip"]').on('click', function () {
            $(this).tooltip('hide')
        })
        return datatable
    },
    titlePage: function (options) {
        $(this).replaceWith( /*html*/ `
            <div class="app-page-title">
                <div class="app-page-title-heading">
                    <div class="app-page-title-icon">
                        <i class="material-icons-outlined icon-gradient" style="background-image: linear-gradient(120deg, ${options.iconColor[0]} 0%, ${options.iconColor[1]} 100%) !important">
                        ${options.icon}
                        </i>
                    </div>
                    <div>
                        ${options.title}
                        <div class="page-title-subheading">${options.subtitle}</div>
                    </div>
                </div>
            </div>
        `);
    },
    topbar: function (options) {
        $(this).load('/src/views/components/topbar/topbar.html', function () {
            $(this).find('.user-info-heading').text(options.user.title);
            $(this).find('.user-info-subheading').text(options.user.subtitle);
            $(this).children(':first').unwrap();
        })
    },
    tableGettingDataIndicator: function () {
        var tableColumntLenght = $(this).find('thead tr:eq(0) th').length;
        $(this).find('tbody').empty();
        $(this).find('tbody').append(
            `<tr>
                <td colspan="${tableColumntLenght}">
                     <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            Please Wait...
                        </div>
                    </div>
                </td>
            </tr>`
        );
    },
    tableNoDataIndicator: function () {
        var tableColumntLenght = $(this).find('thead tr:eq(0) th').length;
        $(this).find('tbody').empty();
        $(this).find('tbody').append(
            `<tr>
                <td colspan="${tableColumntLenght}">
                    No data available in table
                </td>
            </tr>`
        );
    },
    tableUserSetDataFromAPI: async function (api, company, builder) {
        var data = (await api(company)).data;
        if (data == null)
            return;
        if (data.length == 0)
            $(this).tableNoDataIndicator();
        var tableRowHTML = data.map(builder);
        $(this).find('tbody').append(tableRowHTML.join());
    },
    tableSetDataFromAPI: async function (api, builder) {
        $(this).tableGettingDataIndicator();
        var data = (await api()).data;
        if (data == null)
            return;
        if (data.length == 0)
            $(this).tableNoDataIndicator();
        $(this).find('tbody').empty();
        var tableRowHTML = data.map(builder);
        $(this).find('tbody').append(tableRowHTML.join());
    },
    LaporanSetDataFromAPI: async function (api, divisi, prdawal, prdakhir, builder) {
        $(this).tableGettingDataIndicator();
        var data = (await api(divisi, prdawal, prdakhir).data);
        if (data == null)
            return;
        if (data.length == 0)
            $(this).tableNoDataIndicator();
        $(this).find('tbody').empty();
        var tableRowHTML = data.map(builder);
        $(this).find('tbody').append(tableRowHTML.join());
    },
    dashboardSetDataFromAPI: async function (api, search, filter, builder) {
        var data
        if (search != undefined) {
            data = (await api(search, "")).data;
        } else if (filter != undefined) {
            data = (await api("", filter)).data;
        } else {
            data = (await api("", "")).data;
        }

        if (data == null || data.length == 0)
            $(this).empty()
        $(this).empty();
        var tableRowHTML = data.map(builder);
        $(this).append(tableRowHTML.join(''));
    },
    approvalSKISetDataFromAPI: async function (api, periode, jenis, kode, builder) {
        var data
        if (jenis == "SM") {
            data = ((await api(periode)).chart1);
        } else if (jenis == "AM") {
            data = ((await api(periode, kode)).chart8);
        }
        if (data == null)
            return;
        if (data.length == 0)
            $(this).empty();
        $(this).empty();
        var tableRowHTML = data.map(builder);
        $(this).append(tableRowHTML.join(''));
    },
    approvalSKIDetailSetDataFromAPI: async function (api, periode, kode, builder) {
        var data = (await api(periode, kode)).data;
        if (data == null)
            return;
        if (data.length == 0)
            $(this).empty();
        $(this).empty();
        var tableRowHTML = data.map(builder);
        $(this).append(tableRowHTML.join(''));
    },
    mclSetDataFromAPI: async function (api, periode, jenis, kode, builder) {
        var data
        if (jenis == "Header") {
            data = ((await api(periode)).chart2);
        } else if (jenis == "SM") {
            data = ((await api(periode, kode)).chart3);
        } else if (jenis == "AM") {
            data = ((await api(periode, kode)).chart4);
        }
        if (data == null)
            return;
        if (data.length == 0)
            $(this).empty();
        $(this).empty();
        var tableRowHTML = data.map(builder);
        $(this).append(tableRowHTML.join(''));
    },
    dashboardSetModalAPI: async function (api, builder) {
        var data = (await api).data;
        if (data == null || data.length == 0)
            return;
        $(this).empty();
        var tableRowHTML = data.map(builder);
        $(this).append(tableRowHTML.join(''));
    },
    sidebar: function (menu) {
        var menuHTML = '';
        menu.forEach(function (menu) {
            menuHTML += `<div class="vsm-header">${menu.group}</div>`
            var menuGroup = menu.group
            menu.menu.forEach(function (menu) {
                if (menu.name != null) {
                    var collapseId = `collapse` + menuGroup + `${menu.name}`;
                    var subMenuHTML = '';
                    if (menu.submenu.length > 0) {
                        menu.submenu.forEach(function (menu) {
                            subMenuHTML +=
                                `<div class="vsm-item">
                                        <a class="vsm-link route" data-route="${menu.route}">
                                            <span>${menu.name}</span>
                                        </a>
                                    </div>`
                        });
                    }
                    menuHTML +=
                        `<button class="vsm-link route collapsed" data-route="${menu.route}" data-toggle="collapse" data-target="#${collapseId}" aria-expanded="false" aria-controls="${collapseId}">
                                <i class="vsm-icon material-icons-outlined">
                                    ${menu.icon}
                                </i>
                                <span class="vsm-title">${menu.name}</span>`
                    if (menu.submenu.length > 0) {
                        menuHTML += `<i class="vsm-arrow"></i>
                            </button>
                            <div class="vsm-dropdown collapse" id="${collapseId}" data-parent="#vsm-accordion">
                                <div class="vsm-list">${subMenuHTML}</div>
                            </div>`
                    } else {
                        menuHTML += `</button>`
                    }

                }
            });
        });
        $(this).load('/src/views/components/sidebar/sidebar.html', function () {
            $(this).find('#menu').replaceWith(
                `<div class="v-sidebar-menu accordion" id="vsm-accordion">
                   ${menuHTML}
                </div>`
            );
            // initial content
            $('#content').load('src/views/pages/menu/marketing/farmasys/master-dept/master-dept.html');
            $('.route').click(function (e) {
                e.preventDefault();
                $('.route').removeClass('active-item');
                $(this).addClass('active-item');
                const path = $(this).attr('data-route');
                $('#content').load(`src/views/pages/menu/${path}/${path.split('/').last()}.html`, function (response, status, xhr) {
                    if (status == "error") {
                        var msg = "Sorry but there was an error: ";
                        $("#content").html(msg + xhr.status + " " + xhr.statusText);
                    };
                })
            });
            $(this).children(':first').unwrap();
        })
    },
    chartData: async function (api, builder) {
        var data = (await api()).data;
        if (data == null || data.length == 0)
            return;
        $(this).empty();
        var tableRowHTML = data.map(builder);
        $(this).append(tableRowHTML.join(''));
    },
    createChartPeriode: async function (data) {
        var getAset = data.dataAsets;
        var getLabel;
        var getData;
        var getColor;
        var unix;

        data.onSuccess(getAset);
        unix = Date.now();

        var cardChart = /*html*/ `
            <canvas id="cardChart${unix}" class="mb-3" width="200" height="200"></canvas>
        `;
        $(this).html(cardChart);

        getLabel = getAset.map(function (e) {
            return e.label
        });
        dataCheck = getAset.map(function (e) {
            return e.data
        });
        var getData = [];
        dataCheck.forEach(elt => {
            var datas = elt.replace(/,/g, "")
            getData.push(datas);
        });
        getColor = getAset.map(function (e) {
            return e.color
        });
        new Chart($('#cardChart' + unix), {
            type: data.type,
            data: {
                labels: getLabel,
                datasets: [{
                    data: getData,
                    backgroundColor: getColor,
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom',
                }
            }
        });

        getAset.forEach(item => {
            var tableKpiAp =
                `<tr>
                    <td>${item.label}</td>
                    <td>${item.data}</td>
                </tr>`;
            $('#chart-table-' + unix).find('tbody').append(tableKpiAp);
        });
    }
});

Array.prototype.last = function () {
    if (this.length == 0)
        return null
    else
        return this[this.length - 1];
};


String.prototype.dateFormat = function () {
    return moment(this).format('LLL');
};
String.prototype.Format = function () {
    return moment(this).format('LT');
};
String.prototype.ifEmpty = function () {
    return this == "" ? '-' : this;
};
String.prototype.isEmpty = function () {
    return this == "";
}
String.prototype.isNotEmpty = function () {
    return this != "";
}
String.prototype.capitalizeFirstLetterOfEachWord = function () {
    return this.split(/ /g).map(val => val[0].toUpperCase() + val.slice(1)).join(' ')
}
String.prototype.toFloat = function () {
    if (this != '') {
        return parseFloat(this)
    }
    return 0;
}
// String.prototype.toIDR = function () {
//     return 'Rp' + this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
// };

String.prototype.toIDR = function () {
    let proses = this.toString().replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, ".")

    if (proses.indexOf(',') <= 0) {
        proses += ',00'
    }

    return 'Rp. ' + proses
};

function toIDR(angka, prefix) {
    if (angka != null || angka != undefined) {
        var number_string = angka.toString().replace(/[^,\d]/g, ''),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp' + rupiah : '');
    } else {
        return 'Rp0';
    }

}

function formatNumber(n) {
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
    var input_val = input.val();
    if (input_val === "") {
        return;
    }
    var original_len = input_val.length;
    var caret_pos = input.prop("selectionStart");
    if (input_val.indexOf(".") >= 0) {
        var decimal_pos = input_val.indexOf(".");

        var left_side = input_val.substring(0, decimal_pos);
        var right_side = input_val.substring(decimal_pos);

        left_side = formatNumber(left_side);

        right_side = formatNumber(right_side);

        if (blur === "blur") {
            right_side += "00";
        }

        right_side = right_side.substring(0, 2);

        input_val = left_side + "." + right_side;

    } else {
        input_val = formatNumber(input_val);
        // input_val = "Rp" + input_val;

        if (blur === "blur") {
            input_val += ".00";
        }
    }

    input.val(input_val);

    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
}