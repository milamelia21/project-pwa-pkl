var route;
var hostAPI = 'https://api-mkt.farmasys.com/'
//var hostAPI = 'http://localhost:8088/'

//var hostPrint = 'http://127.0.0.1:5502/'
var hostPrint = 'https://app.farmasys.com/'

class Navigator {
  static push(path) {
    location.href = "/#" + path
  }

  static back() {
    history.back()
  }
}



function parseJwt(access_token) {
  var base64Url = access_token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));

  return JSON.parse(jsonPayload);
}
// console.log(parseJwt(access_token))

function convertMenuFromJSON(json) {
  var resultMenu = []
  json.forEach(elementJSON => {
    var indexOfGroupMenu = resultMenu.findIndex(elementGroupMenu => elementGroupMenu.group == elementJSON.GroupMenu)
    if (indexOfGroupMenu == -1) {
      resultMenu.push({
        'group': elementJSON.GroupMenu,
        'menu': [{
          name: elementJSON.Menu,
          icon: elementJSON.Icon,
          submenu: [{
            name: elementJSON.SubMenu,
            route: elementJSON.PathUrl,
            permission: [elementJSON.Akses_Show, elementJSON.Akses_Add, elementJSON.Akses_Edit, elementJSON.Akses_Delete, elementJSON.Akses_Print],
          }]
        }]
      })
    } else {
      var groupMenu = resultMenu[indexOfGroupMenu]
      var indexOfMenu = groupMenu.menu.findIndex(elementMenu => elementMenu.name == elementJSON.Menu)
      if (indexOfMenu == -1) {
        groupMenu.menu.push({
          name: elementJSON.Menu,
          icon: elementJSON.Icon,
          submenu: [{
            name: elementJSON.SubMenu,
            route: elementJSON.PathUrl,
            permission: [elementJSON.Akses_Show, elementJSON.Akses_Add, elementJSON.Akses_Edit, elementJSON.Akses_Delete, elementJSON.Akses_Print],
          }]
        })
      } else {
        groupMenu.menu[indexOfMenu].submenu.push({
          name: elementJSON.SubMenu,
          route: elementJSON.PathUrl,
          permission: [elementJSON.Akses_Show, elementJSON.Akses_Add, elementJSON.Akses_Edit, elementJSON.Akses_Delete, elementJSON.Akses_Print],
        });
      }
    }
  });
  return resultMenu;
}